#include "SpaceShip.h"
#include <iostream>
SpaceShip::SpaceShip(const sf::FloatRect & boundary, const sf::Vector2f & location, const float & mass) : Particle(boundary, location, mass)
{
	this->mainRocket = sf::CircleShape(20); // radius of 20
	this->mainRocket.setPointCount(3);
	this->mainRocket.setOrigin(20, 20); // diameter is 40
	this->mainRocket.setFillColor(sf::Color(127, 127, 127));
	this->mainRocket.setOutlineColor(sf::Color::White);
	this->mainRocket.setOutlineThickness(1);

	this->leftThruster = sf::RectangleShape(sf::Vector2f(5, 5));
	this->leftThruster.setOrigin(15, -10+2.5);
	this->leftThruster.setFillColor(sf::Color(127, 127, 127));
	this->leftThruster.setOutlineColor(sf::Color::White);
	this->leftThruster.setOutlineThickness(1);

	this->rightThruster = sf::RectangleShape(sf::Vector2f(5, 5));
	this->rightThruster.setOrigin(15, 10+2.5);
	this->rightThruster.setFillColor(sf::Color(127, 127, 127));
	this->rightThruster.setOutlineColor(sf::Color::White);
	this->rightThruster.setOutlineThickness(1);

	thrust.x = 0.0f;
	thrust.y = -0.001f;

	angle = 0;

	// Remember that PI == 180 degrees so PI/2 = 90 degrees

	// set the ship so that it is going up the screen
	thrust = vm::setAngle(thrust, -std::_Pi / 2); // angle is measured in radians note that the unit circle has a different orientation than the usual math orientation
	angle = vm::getAngle(thrust);
}

SpaceShip::SpaceShip() : Particle()
{
	this->mainRocket = sf::CircleShape(2);

	this->leftThruster = sf::RectangleShape();
	this->rightThruster = sf::RectangleShape();
}

SpaceShip::~SpaceShip()
{
}

sf::CircleShape& SpaceShip::getCircleShape()
{
	return this->mainRocket;
}

void SpaceShip::setCircleShape(sf::CircleShape & circle)
{
	this->mainRocket = circle;
}

void SpaceShip::rotateLeft()
{
	angle = vm::getAngle(thrust) - 0.05f;
	thrust = vm::setAngle(thrust, angle); // change heading
}

void SpaceShip::rotateRight()
{
	angle = vm::getAngle(thrust) + 0.05f;
	thrust = vm::setAngle(thrust, angle); // change heading
}

void SpaceShip::fireRocket()
{
	thrust = vm::setMagnitude(thrust, 5.0f);

	applyForce(thrust);

	leftThruster.setFillColor(sf::Color::Red);
	rightThruster.setFillColor(sf::Color::Red);
}

void SpaceShip::stop()
{
	leftThruster.setFillColor(sf::Color(127, 127, 127));
	rightThruster.setFillColor(sf::Color(127, 127, 127));

	setAcceleration(sf::Vector2f());
}

void SpaceShip::draw(sf::RenderWindow * window)
{
	// convert between radians and degrees our math library uses radians whereas SFML needs degrees
	float degrees = angle * 180 / std::_Pi;

	std::cout << velocity.x << "," << velocity.y << std::endl;
	std::cout << "degrees:" << degrees << std::endl;

	// rotate our ship
	mainRocket.setRotation(degrees-30); // offset of the main rocket from the thrusters
	leftThruster.setRotation(degrees);
	rightThruster.setRotation(degrees);

	// draw the component parts of our ship
	window->draw(mainRocket);
	window->draw(leftThruster);
	window->draw(rightThruster);
}

void SpaceShip::update(const sf::Time &deltaTime)
{
	Particle::update(deltaTime);
	mainRocket.setPosition(location);
	leftThruster.setPosition(location);
	rightThruster.setPosition(location);
}
