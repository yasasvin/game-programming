#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <random>
#include <list>
#include <cmath>

#include "Particle.h"
#include "VectorMath.h"
#include "Constraint.h"

const int PARTICLE_SZ = 10;
const int PARTICLE_RADIUS = 15;
const int SPEED = 200;

const int WIDTH = 1024;
const int HEIGHT = 768;

std::default_random_engine generator;
std::uniform_real_distribution<float> vx(-SPEED, +SPEED);
std::uniform_real_distribution<float> vy(-SPEED, +SPEED);

std::uniform_real_distribution<float> x(0,WIDTH);
std::uniform_real_distribution<float> y(0,HEIGHT);

Particle& createParticle(std::list <Particle> &particles, const sf::FloatRect &rect, sf::RenderWindow &window, const sf::Vector2f &location)
{
	sf::Color colour = sf::Color::Red;

	Particle particle = Particle(rect, location, 1);
	sf::CircleShape circle = particle.getCircleShape();

	circle.setRadius(2);
	circle.setFillColor(colour);
	particle.setCircleShape(circle);

	particles.push_front(particle);

	return particle;
}

void createParticle(std::list <Particle> &particles, const sf::FloatRect &rect, sf::RenderWindow &window)
{
	sf::Color colour = sf::Color::Green;

	Particle particle = Particle(rect, sf::Vector2f(x(generator),y(generator)), 1);
	sf::CircleShape circle = particle.getCircleShape();

	circle.setRadius(PARTICLE_RADIUS);
	circle.setFillColor(colour);
	particle.setCircleShape(circle);

	particles.push_front(particle);
}


void processEvents(std::list <Particle> &particles, const sf::FloatRect &rect, sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
	}

}

int main()
{
	float angle = 0;
	sf::Time TimePerFrame = sf::seconds(1.f / 60);

	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;

	sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "SFML Verlet Collision Detection", sf::Style::Default, settings);
	window.setFramerateLimit(60);


	std::list <Constraint> constraints;
	std::list <Particle> particles;

	const sf::FloatRect rect = sf::FloatRect(sf::Vector2f(0, 0), sf::Vector2f(window.getSize()));

	float left = 200;
	float top = 200;

	float height = 100;
	float width = 100;

	createParticle(particles, rect, window, sf::Vector2f(left, top));
	Particle &leftTop = particles.front();
	leftTop.setAcceleration(sf::Vector2f(5.0f, 10.0f));

	createParticle(particles, rect, window, sf::Vector2f(left + width, top));
	Particle &rightTop = particles.front();

	createParticle(particles, rect, window, sf::Vector2f(left, top+height));
	Particle &leftBottom = particles.front();

	createParticle(particles, rect, window, sf::Vector2f(left +width, top +height));
	Particle &rightBottom = particles.front();
	
	constraints.push_back(Constraint(leftTop, rightTop,width));
	constraints.push_back(Constraint(rightTop, rightBottom, height));
	constraints.push_back(Constraint(rightBottom,leftBottom, width));
	constraints.push_back(Constraint(leftBottom, leftTop, height));
	constraints.push_back(Constraint(rightTop, leftBottom,sqrt(width * width + height * height)));

	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;
	while (window.isOpen())
	{
		processEvents(particles, rect, window);

		timeSinceLastUpdate += clock.restart();

		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;
			processEvents(particles, rect, window);

			for(std::list<Particle>::iterator it = particles.begin(); it != particles.end(); ++it)
			{
				it->checkEdges();
				it->update(TimePerFrame);
			}

			for (std::list<Constraint>::iterator it = constraints.begin(); it != constraints.end(); ++it)
			{
				it->update();
			}
		}

		window.clear();


		// draw particles
		for (std::list<Particle>::iterator it = particles.begin(); it != particles.end(); ++it)
		{
			sf::CircleShape circle = it->getCircleShape();
			window.draw(circle);
		}

		// draw the constraints
		for (std::list<Constraint>::iterator it = constraints.begin(); it != constraints.end(); ++it)
		{
			sf::Vertex vertices[2] =
			{
				sf::Vertex(it->getParticle1().getCurrentPosition(),sf::Color::Red),
				sf::Vertex(it->getParticle2().getCurrentPosition(),sf::Color::Red)
			};

			window.draw(vertices, 2, sf::Lines);
		}
		window.display();
	}
}

