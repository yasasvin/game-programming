#include "Particle.h"

Particle::Particle()
{
	current = sf::Vector2f(0, 0);
	previous = sf::Vector2f(0.0000001, 0.0000001);
	acceleration = sf::Vector2f(0, 0);
	boundary = sf::FloatRect();
}

Particle::Particle(const sf::FloatRect &boundary, const  sf::Vector2f &location,const  float &mass)
{
	this->current = location;
	this->previous = location -sf::Vector2f(0.05, 0.05);
	this->acceleration = sf::Vector2f();
	this->boundary = boundary;
}

void Particle::setAcceleration(const sf::Vector2f &acceleration)
{
	this->acceleration = acceleration;
}

sf::Vector2f Particle::getAcceleration()
{
	return acceleration;
}

sf::Vector2f& Particle::getCurrentPosition()
{
	return current;
}

sf::Vector2f & Particle::getPreviousPosition()
{
	return this->previous;
}

void Particle::update(const sf::Time &deltaTime)
{
	float time = deltaTime.asSeconds();

	sf::Vector2f velocity = current - previous;

	previous = current;
	current = current + velocity + (acceleration * (time * time));
	
	circle.setPosition(current);
}

void Particle::checkEdges()
{
	// reverse the direction

	sf::Vector2f velocity = current - previous;

	if ((current.x > boundary.width))
	{
		current.x = boundary.width;
		previous.x = current.x + velocity.x;
	}

	if ((current.x < 0))
	{
		current.x = 0;
		previous.x = current.x + velocity.x;

	}

	if ((current.y > boundary.height))
	{
		current.y = boundary.height;
		previous.y = current.y + velocity.y;
	}

	if (current.y < 0)
	{
		current.y = 0;
		previous.y = current.y + velocity.y;
	}
}

Particle::~Particle()
{
}

sf::CircleShape& Particle::getCircleShape()
{
	return circle;
}
void Particle::setCircleShape(sf::CircleShape& circle)
{
	this->circle = circle;
}