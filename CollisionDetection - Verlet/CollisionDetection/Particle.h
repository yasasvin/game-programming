#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>
#include <SFML\Graphics\Rect.hpp>
#include "VectorMath.h"
#include <list>
#pragma once
class Particle
{
	protected:
		sf::CircleShape circle;
		sf::Vector2f previous;
		sf::Vector2f current;
		
		sf::Vector2f acceleration; // the rate of change in velocity
		sf::FloatRect boundary;
	public:
		sf::CircleShape& getCircleShape();
		void setCircleShape(sf::CircleShape& circle);
		
		void setAcceleration(const sf::Vector2f &acceleration);
		sf::Vector2f getAcceleration();

		sf::Vector2f& getCurrentPosition();
		sf::Vector2f& getPreviousPosition();
		virtual void update(const sf::Time &deltaTime);
		void checkEdges();

		Particle(const sf::FloatRect &boundary, const  sf::Vector2f &location, const float &mass);
		Particle();
		~Particle();
};

