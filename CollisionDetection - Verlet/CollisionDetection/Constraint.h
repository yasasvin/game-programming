#include "Particle.h"
#pragma once
class Constraint
{
	Particle &particle1;
	Particle &particle2;
	
	float restlength = 20;

	public:
		Constraint(Particle &particle1,Particle &particle2);
		Constraint(Particle &particle1, Particle &particle2, float length);
		~Constraint();

		Particle &getParticle1();
		Particle &getParticle2();

		void update();
};

