
#pragma once
#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>

float scale = 0.01;
float crateWidth = 20;

float scaledWidth;
float scaledHeight;

float canvasYToBox2D(float y)
{
	float box2d = ((scaledHeight / 2) - (y * scale));
	return box2d;
}

float canvasXToBox2D(float x)
{
	float box2d = (x * scale) - (scaledWidth / 2);
	return box2d;
}

float box2DYToCanvas(float y) {
	return (scaledHeight - (y + (scaledHeight / 2.0f))) / scale;
}

float box2DXToCanvas(float x)
{
	return (x + (scaledWidth / 2.0f)) / scale;
}

int main()
{
	b2Vec2 gravity(0.0f, -10.0f);

	float TIMESTEP = 1.0f / 60.0f;
	int VELOCITY = 4;
	int POSITION = 2;

	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;

	sf::RenderWindow window(sf::VideoMode(600, 600), "Box2D Example");

	scaledWidth = scale * (window.getSize().x);
	scaledHeight = scale * (window.getSize().y);

	b2World world = b2World(gravity);
	world.SetAllowSleeping(false);

	b2PolygonShape groundBox;
	groundBox.SetAsBox(scaledWidth / 2.0f /* half width */, 0.1f /* half height */);

	b2FixtureDef groundFixture;
	groundFixture.shape = &groundBox;
	groundFixture.density = 1.0f;
	groundFixture.friction = 0.4f;

	b2BodyDef bd;
	bd.type = b2BodyType::b2_staticBody;
	bd.position.Set(0.0f, -(scaledHeight/2.0f)); // with respect to the center of mass of our polygon shape

	b2Body *body = world.CreateBody(&bd);
	body->CreateFixture(&groundFixture);

	window.setFramerateLimit(60); // control how fast the screen is refreshed (fps)

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}
			else
			{
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left) || sf::Event::MouseMoved)
				{
					sf::Vector2i mouse = sf::Mouse::getPosition(window);

					b2BodyDef crate;
					crate.type = b2BodyType::b2_dynamicBody;
					crate.position.Set(canvasXToBox2D(mouse.x), canvasYToBox2D(mouse.y));

					b2PolygonShape crateShape;
					crateShape.SetAsBox((crateWidth * scale) / 2.0f, (crateWidth * scale) / 2.0f);

					b2FixtureDef crateFd;
					crateFd.shape = &crateShape;
					crateFd.density = 1.0f;
					crateFd.friction = 0.3f;
					crateFd.restitution = 0.2f;

					b2Body *crateBody = world.CreateBody(&crate);
					crateBody->CreateFixture(&crateFd);

				}
			}
		}
		window.clear();
		world.Step(TIMESTEP, VELOCITY, POSITION);

		b2Body *body = world.GetBodyList();
		while (body != NULL)
		{
			b2Vec2 position = body->GetPosition();		
			b2Fixture *fixture = body->GetFixtureList();

			while (fixture != NULL)
			{
				switch (fixture->GetType())
				{
					case b2Shape::e_polygon:
					{
						b2PolygonShape* poly = (b2PolygonShape*)fixture->GetShape();

						sf::ConvexShape convex;
						convex.setOutlineColor(sf::Color::White);
						convex.setFillColor(sf::Color::Red);
						convex.setOutlineThickness(1.0f);
						convex.setPointCount(poly->GetVertexCount());

						for (int32 i = 0; i < poly->GetVertexCount();i++)
						{
							const b2Vec2 src = poly->GetVertex(i); // local vertex around 0,0
							const b2Vec2 world = body->GetWorldPoint(src); // convert to a global world point

							float x = box2DXToCanvas(world.x);
							float y = box2DYToCanvas(world.y);

							convex.setPoint(i, sf::Vector2f(x, y));
						}
						window.draw(convex);
					}
					break;
				}
				fixture = fixture->GetNext();				
			}
			body = body->GetNext();
		}

		window.display();
	}

	return 0;
}
