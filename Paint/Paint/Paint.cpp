#include "stdafx.h"
#include "Paint.h"

void paintScreen(sf::RenderWindow &window, std::list <sf::CircleShape> &circleShapes, sf::Text &text, int x, int y)
{
	std::stringstream ss;

	ss << x << "," << y;

	text.setString(ss.str());

	sf::CircleShape circle(20);
	circle.setFillColor(sf::Color(sf::Color::Green));
	circle.setPosition(sf::Vector2f(x, y));

	circleShapes.push_front(circle);
}

int main()
{
	int data[5000];
	int dataIndex = 0;

	

	std::list <sf::CircleShape> circleShapes;

	sf::CircleShape circle(20);
	circle.setFillColor(sf::Color(sf::Color::Green));

	sf::Font font;
	

	bool fontLoad = font.loadFromFile("arial.ttf");

	if (!fontLoad)
	{
		std::cout << "Error" << std::endl;
	}

	sf::Text text;
	text.setFont(font);
	text.setCharacterSize(10);
	text.setColor(sf::Color::White);
	text.setOrigin(sf::Vector2f(0, 0));
	text.setPosition(sf::Vector2f(0, 0));


	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML Vector Paint");
	sf::Vector2f pos;

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}

			if (event.type == sf::Event::MouseButtonPressed)
			{
				if (event.mouseButton.button == sf::Mouse::Left)
				{
					paintScreen(window,circleShapes,text,event.mouseButton.x, event.mouseButton.y);
				}
			}

			if (event.type == sf::Event::MouseMoved)
			{
				paintScreen(window, circleShapes, text, event.mouseMove.x, event.mouseMove.y);
			}
		}

		window.clear();
		window.draw(text);
		
		for (std::list<sf::CircleShape>::iterator it = circleShapes.begin(); it != circleShapes.end(); ++it)
		{
			window.draw(*it);
		}

		window.display();
	}
}