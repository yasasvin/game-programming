#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>
#include <SFML\Graphics\Rect.hpp>
#include "VectorMath.h"

#pragma once
class Particle
{
	protected:
		sf::Vector2f previous;
		sf::Vector2f current;
		
		sf::Vector2f acceleration; // the rate of change in velocity
		float mass;
		float topSpeed;
		sf::FloatRect boundary;
		int life = 255;
	public:
		void setAcceleration(const sf::Vector2f &acceleration);
		sf::Vector2f getAcceleration();

		int getLife();
		void applyForce(const sf::Vector2f &force);

		sf::Vector2f getCurrentPosition();
		virtual void update(const sf::Time &deltaTime);
		void checkEdges();

		Particle(const sf::FloatRect &boundary, const  sf::Vector2f &location, const float &mass);
		Particle();
		~Particle();
};

