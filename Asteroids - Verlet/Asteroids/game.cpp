#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <random>
#include <list>
#include <cmath>
#include "Particle.h"
#include "SpaceShip.h"
#include "VectorMath.h"

bool right;
bool left;
bool rocket;

// process the state machine to turn on and off the different rocket thrusters 
void processEvents(Particle &particles, const sf::FloatRect &rect, sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			left = true;
			right = false;
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			left = false;
			right = true;
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			left = false;
			right = false;
			rocket = true;
		}
		else if (event.type == sf::Event::KeyReleased)
		{
			left = false;
			right = false;
			rocket = false;
		}
	}
}

int main()
{
	sf::Vector2f thrust(0,-0.001f);

	float angle = 0;
	sf::Time TimePerFrame = sf::seconds(1.f / 60);

	sf::ContextSettings context;
	context.antialiasingLevel = 10;

	sf::RenderWindow window(sf::VideoMode(1024, 768), "SFML Asteroids", sf::Style::Default, context);
	window.setFramerateLimit(60);


	const sf::FloatRect rect = sf::FloatRect(sf::Vector2f(0, 0), sf::Vector2f(window.getSize()));

	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;

	SpaceShip ship = SpaceShip(rect, sf::Vector2f(window.getSize().x / 2, window.getSize().y / 2), 1);

	// set the ship so that it is going up the screen
	thrust = vm::setAngle(thrust,-std::_Pi / 2);

	angle = vm::getAngle(thrust);

	while (window.isOpen())
	{
		if (left)
		{
			angle = vm::getAngle(thrust) - 0.05;
		}

		if (right)
		{
			angle = vm::getAngle(thrust) + 0.05;
		}

		thrust = vm::setAngle(thrust, angle);

		if (rocket)
		{
			thrust = vm::setMagnitude(thrust, 5.0f);

			ship.applyForce(thrust);

			ship.getLeftThruster().setFillColor(sf::Color::Red);
			ship.getRightThruster().setFillColor(sf::Color::Red);
		}
		else
		{
			ship.getLeftThruster().setFillColor(sf::Color(127, 127, 127));
			ship.getRightThruster().setFillColor(sf::Color(127, 127, 127));

			ship.setAcceleration(sf::Vector2f());
		}

		float degrees = angle * 180 / std::_Pi;

		processEvents(ship, rect, window);

		timeSinceLastUpdate += clock.restart();

		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;
			processEvents(ship, rect, window);

			ship.update(TimePerFrame);
			ship.checkEdges();
		}

		window.clear();
		sf::CircleShape circle = ship.getCircleShape();

		sf::Color random = circle.getFillColor();
		circle.setFillColor(random);
		circle.rotate(degrees);

		ship.getLeftThruster().setRotation(degrees);
		ship.getRightThruster().setRotation(degrees);

		window.draw(circle);

		window.draw(ship.getLeftThruster());
		window.draw(ship.getRightThruster());

		window.display();
	}
}

