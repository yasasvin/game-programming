#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <random>
#include <list>
#include "Particle.h"
#include "VectorMath.h"

const int PARTICLE_SZ = 5;
const int PARTICLE_RADIUS = 5;

std::default_random_engine generator;
std::normal_distribution<float> velocity(1, 50);
std::uniform_int_distribution<int> direction(0, 1);
std::uniform_int_distribution<int> color(0, 255);

void createFireworks(std::list <Particle> &particles, const sf::FloatRect &rect, sf::RenderWindow &window, const sf::Vector2f &location)
{
	sf::Color random = sf::Color::Red; //(color(generator), color(generator), color(generator));
	for (int i = 0; i < PARTICLE_SZ; i++)
	{
		Particle particle = Particle(rect, location, 1);

		float x = velocity(generator);

		if (!direction(generator))
		{
			x = x * -1;
		}

		float y = velocity(generator);

		if (!direction(generator))
		{
			y = y * -1;
		}

		particle.setVelocity(sf::Vector2f(x, y));
		particle.applyForce(sf::Vector2f(0, 90.8));

		sf::CircleShape circle = particle.getCircleShape();

		circle.setRadius(PARTICLE_RADIUS);
		circle.setFillColor(random);

		particle.setCircleShape(circle);
		particles.push_front(particle);
	}
}


void processEvents(std::list <Particle> &particles, const sf::FloatRect &rect, sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
		else if (event.type == sf::Event::MouseMoved)
		{
			createFireworks(particles, rect, window, sf::Vector2f(event.mouseMove.x, event.mouseMove.y));
		}
	}

}

int main()
{
	sf::Time TimePerFrame = sf::seconds(1.f / 60.f);

	sf::RenderWindow window(sf::VideoMode(1024, 768), "SFML Particle Trails!");
	window.setFramerateLimit(60);

	std::list <Particle> particles;

	const sf::FloatRect rect = sf::FloatRect(sf::Vector2f(0, 0), sf::Vector2f(window.getSize()));

	createFireworks(particles, rect, window, sf::Vector2f(window.getSize().x / 2, window.getSize().y / 2));

	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;
	while (window.isOpen())
	{
		processEvents(particles, rect, window);

		timeSinceLastUpdate += clock.restart();

		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;
			processEvents(particles, rect, window);

			for(std::list<Particle>::iterator it = particles.begin(); it != particles.end(); ++it)
			{
				it->update(TimePerFrame);
				it->checkEdges();
			}
		}

		window.clear();

		std::cout <<"sz:" << particles.size() << std::endl;

		for (std::list<Particle>::iterator it = particles.begin(); it != particles.end(); ++it)
		{
			sf::CircleShape circle = it->getCircleShape();

			sf::Color random = circle.getFillColor();
			random.a = it->getLife();
			circle.setFillColor(random);

			window.draw(circle);

		}
		for (std::list<Particle>::iterator it = particles.begin(); it != particles.end();)
		{
			if (it->getLife() < 1)
			{
				it = particles.erase(it);
			}
			else
			{
				++it;
			}
		}
		window.display();		
	}
}

