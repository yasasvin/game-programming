#include <SFML/Graphics.hpp>
#include <iostream>
#include <cmath>

int main()
{

	sf::ContextSettings context;
	context.antialiasingLevel = 10;


	// create the window
	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML Tank movement", sf::Style::Default,context);
	window.setFramerateLimit(60);

	sf::Sprite sprite;

	sprite.setPosition(sf::Vector2f(window.getSize().x / 2, window.getSize().y / 2));

	sf::Texture tank;
	if (!tank.loadFromFile("assets/tank_sprite.png"))
	{
		std::cout << "Error loading resource tank_sprite.png" << std::endl;
	}

	sf::Texture tileset;
	if (!tileset.loadFromFile("assets/tile_set.png"))
	{
		std::cout << "Error loading resource tile_set.png" << std::endl;
	}

	sf::Image tankImage = tank.copyToImage();
	tankImage.createMaskFromColor(sf::Color(255, 0, 255), 0);

	tank.loadFromImage(tankImage);

	tank.setSmooth(true);

	sprite.setTexture(tank);
	//sprite.setScale(sf::Vector2f(0.75f, 0.75f));
	sprite.setPosition(400, 300);
	sprite.setOrigin(60, 75); // put it in the center

	int frame = 12; // start it off at angle 0
	int frameMax = 24;
	int frameDirection = 1;

	bool up = false;
	bool down = false;
	bool left = false;
	bool right = false;

	float angle = 0;

	// run the program as long as the window is open
	while (window.isOpen())
	{
		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (window.pollEvent(event))
		{
			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			{
				left = true;
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			{
				right = true;
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			{
				up = true;
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			{
				down = true;
			}
		}
		

		if (left)
		{
			frameDirection = -1;
			frame = frame + frameDirection;

			angle = angle + M_PI / 12;

			left = false;
		}
		else if(right)
		{
			frameDirection = 1;
			frame = frame + frameDirection;

			angle = angle - M_PI / 12;

			right = false;
		}

		if (frame == 12)
		{
			angle = 0;
		}
		if (angle < 0)
		{
			angle = 2 * M_PI;
		}

		std::cout << "frame:" << frame << std::endl;
		std::cout << "angle:" << angle << std::endl;

		sf::Vector2f tankPos = sprite.getPosition();
		if (up)
		{
			tankPos.x = tankPos.x + (std::cos(angle)*10);
			tankPos.y = (tankPos.y + (-1* (std::sin(angle)*10)));

			sprite.setPosition(tankPos);

			up = false;
		}
		if (down)
		{
			tankPos.x = tankPos.x + (std::cos(angle) * -10);
			tankPos.y = tankPos.y + ((std::sin(angle) * 10));

			sprite.setPosition(tankPos);

			down = false;

		}

		std::cout << "x:" << tankPos.x << std::endl;
		std::cout << "y:" << tankPos.y << std::endl;

		if (frame < 0)
		{
			frame = frameMax - 1;
		}
		else if (frame == frameMax)
		{
			frame = 0;
		}

		sprite.setTextureRect(sf::IntRect(frame * 120, 0, 120, 150));

		// clear the window with black color
		window.clear(sf::Color::Black);

		// draw everything here...
		window.draw(sprite);

		// end the current frame
		window.display();
	}

	return 0;
}