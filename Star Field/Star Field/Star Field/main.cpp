#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <iostream>
#include <random>
#include <list>
#include <cmath>

float SCALE = 400;
float DISTANCE = 3.0f;

const int WIDTH = 800;
const int HEIGHT = 600;

std::default_random_engine generator;
std::uniform_real_distribution<float> x(-30, +30);
std::uniform_real_distribution<float> y(-30, +30);
std::uniform_real_distribution<float> z(-60, +60);

void processEvents(sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
	}
}

float cartesianXToScreenX(float cartesianX, float width)
{
	return cartesianX + (width / 2.0f);
}

float cartesianYToScreenY(float cartesianY, float height)
{
	return (height / 2) - cartesianY;
}

float project(float world_point, float z)
{
	return world_point / (z + DISTANCE);
}

sf::Vector2f convert_point(sf::Vector3f point)
{
	float x = project(point.x, point.z) * SCALE;
	float y = project(point.y, point.z) * SCALE;

	float translateX = cartesianXToScreenX(x, WIDTH);
	float translateY = cartesianYToScreenY(y, HEIGHT);

	sf::Vector2f pnt;
	pnt.x = translateX;
	pnt.y = translateY;

	return pnt;
}

int main()
{
	std::list <sf::Vector3f> points;

	for (int i = 0; i < 1000; i++)
	{
		sf::Vector3f point;
		point.x = x(generator);
		point.y = y(generator);
		point.z = z(generator);

		points.emplace_back(point);
	}


	sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "SFML 3D Stars");
	window.setFramerateLimit(60);

	while (window.isOpen())
	{
		processEvents(window);
		window.clear();

		for (std::list<sf::Vector3f>::iterator it = points.begin(); it != points.end(); ++it)
		{			
			sf::CircleShape point;
			point.setPointCount(3);
			point.setRadius(1);
			//point.setFillColor(sf::Color::Green);
			point.setOutlineColor(sf::Color::Green);

			sf::Vector3f world_point = *it;
			sf::Vector2f screen_point = convert_point(world_point);

			point.setPosition(screen_point);

			float opacity = 255.0f - ((it->z - 30) / 60.0f);

			point.setFillColor(sf::Color(0,255,0,opacity));

			window.draw(point);

			it->z = it->z + 2;

			if (it->z > 40)
			{
				it->x = x(generator);
				it->y = y(generator);
				it->z = z(generator);
			}
		}
		// DISTANCE = DISTANCE - 0.5f;
		window.display();
	}
}

