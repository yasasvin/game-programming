#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <iostream>
#include <random>
#include <list>
#include <cmath>

const int WIDTH = 800;
const int HEIGHT = 600;

void processEvents(sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
	}
}

int main()
{
	float angle = 0;
	float RADIUS = 200;

	bool triangle = true;

	sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "SFML Unit Circle");
	window.setFramerateLimit(60);

	while (window.isOpen())
	{
		processEvents(window);

		window.clear();

		// calculate the co-ordinates of the point on the circle using trigonometry
		double x = std::cos(angle)* RADIUS;
		double y = std::sin(angle)* RADIUS;
		
		// offset the origin point
		double xpos = x + (WIDTH / 2.0f);
		double ypos = y + (HEIGHT / 2.0f);

		// calculate the centre of the screen
		double centreX = WIDTH / 2.0;
		double centreY = HEIGHT / 2.0;

		sf::CircleShape outline;
		outline.setOutlineColor(sf::Color::White);
		outline.setOutlineThickness(4);
		outline.setFillColor(sf::Color::Black);
		outline.setPosition(centreX,centreY);
		outline.setOrigin(RADIUS, RADIUS);
		outline.setRadius(RADIUS);
		outline.setPointCount(100);

		window.draw(outline);

		sf::CircleShape shape;
		shape.setFillColor(sf::Color::Red);
		shape.setPosition(xpos, ypos);
		shape.setOrigin(10, 10);
		shape.setRadius(10);

		window.draw(shape);

		if (triangle)
		{
			// draw the spoke from centre to the outer circle
			sf::Vertex hyp[] =
			{
				sf::Vertex(sf::Vector2f(centreX, centreY)),
				sf::Vertex(sf::Vector2f(xpos, ypos))
			};
			window.draw(hyp, 2, sf::Lines);

			// draw the opposite side in terms of trig
			sf::Vertex opp[] =
			{
				sf::Vertex(sf::Vector2f(xpos, ypos)),
				sf::Vertex(sf::Vector2f(xpos, centreY))
			};
			window.draw(opp, 2, sf::Lines);

			// draw the adjacent side in terms of trig
			sf::Vertex adj[] =
			{
				sf::Vertex(sf::Vector2f(centreX, centreY)),
				sf::Vertex(sf::Vector2f(xpos, centreY))
			};
			window.draw(adj, 2, sf::Lines);
		}

		// sin, cos are cyclic functions limit does not matter
		angle = angle - 0.01;

		window.display();
	}
}

