#include "SFML/Graphics.hpp"
#include "SFML/OpenGL.hpp" // SFML OpenGL include
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

class Mesh
{
	private:
		std::vector <GLfloat> vertices;
		std::vector <unsigned int> faces;
		std::vector <unsigned int> normalIndex;
	public:
		Mesh()
		{

		}
		void draw()
		{
			glEnableClientState(GL_VERTEX_ARRAY);
			glVertexPointer(3, GL_FLOAT, 0, &vertices[0]);
			glDrawElements(GL_TRIANGLES, faces.size(), GL_UNSIGNED_INT, &faces[0]);
			glDisableClientState(GL_VERTEX_ARRAY);

		}
		void load(const char* filename)
		{
			std::ifstream file(filename);
			
			std::string line;
			while (std::getline(file, line))
			{
				std::istringstream data(line);
				
				std::string prefix;
				data >> prefix;

				if (prefix == "#")
				{
					// comment don't do anything
					continue;
				}
				else if (prefix == "mtllib")
				{
					// material file not implemented yet
					continue;
				}
				else if (prefix == "usemtl")
				{
					// material definition not implemented yet
					continue;
				}
				else if (prefix == "o")
				{
					// object name ignored
					continue;
				}
				else if (prefix == "s")
				{
					// smooth shading ignored
					continue;
				}
				else if (prefix == "v") // process vertex
				{
					for (int i = 0;i < 3; i++)
					{
						GLfloat tmp;
						data >> tmp;
						vertices.push_back(tmp);
					}
				}
				else if (prefix == "f") // process face
				{
					for (int i = 0;i < 3; i++)
					{
						std::string face;
						data >> face;

						std::istringstream faceData(face);
						std::string vertex;

						while (std::getline(faceData, vertex, '/'))
						{
							std::istringstream vertexData(vertex);

							unsigned int tmp;
							vertexData >> tmp;
							faces.push_back(tmp - 1);
							break; //TODO implement texture co-ordinates and normals
						}
					}
				}
				else
				{
					// ignore anything else
					continue;
				}
			}

		}
};

int main()
{
	// Create the main window
	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML OpenGL");

	Mesh mesh = Mesh();
	//mesh.load("dodecahedron.obj");
	mesh.load("suzanne.obj");
	//cube.load("f-16.obj");
	//cube.load("minicooper.obj");

	//cube.load("tyra.obj");
	//cube.load("bunny.obj");

	// Create a clock for measuring time elapsed
	sf::Clock Clock;

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // nicest perspective correction calculations

	//prepare OpenGL surface for HSR
	glClearDepth(1.f); // clear the z-buffer/depth buffer completely +1.0f is the furtherest away
	glClearColor(0.3f, 0.3f, 0.3f, 0.f); // set the background colour for when we clear the screen RGBA values in the 0.0 to 1.0 range. This gives us a nice grey background.

	float z=0; // z-axis position

	// Setup a perspective projection & Camera position

	// GL_PROJECTION what we actually see
	glMatrixMode(GL_PROJECTION); // Select the builtin projection matrix
	glLoadIdentity();  // reset the projection matrix by loading the projection identity matrix

	GLdouble fovY = 90;
	GLdouble aspect = 1.0f;
	GLdouble zNear = 1.0f; 
	GLdouble zFar = 300.0f;

	const GLdouble pi = 3.1415926535897932384626433832795;
	GLdouble fW, fH;

	fH = tan(fovY / 360 * pi) * zNear;
	fW = fH * aspect;

	// define a perspective projection
	glFrustum(-fW, fW, -fH, fH, zNear, zFar); // multiply the set matrix; by a perspective matrix

	bool rotate = true;
	float angleX = 1;
	float angleY = 1;

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glShadeModel(GL_FLAT);

	GLfloat DIFFUSE_COLOR[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat SPECULAR_COLOR[] = { 1.25f, 1.25f, 1.25f, 1.0f };
	GLfloat LIGHT_POSITION[] = { 0.0f, -5, -2, 1.0f };
	glLightfv(GL_LIGHT0, GL_POSITION, LIGHT_POSITION);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, DIFFUSE_COLOR);
	glLightfv(GL_LIGHT0, GL_SPECULAR, SPECULAR_COLOR);

	// Start game loop
	while (window.isOpen())
	{
		// Process events
		sf::Event event;
		while (window.pollEvent(event))
		{
			// Close window : exit
			if (event.type == sf::Event::Closed)
				window.close();

			// Escape key : exit
			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Escape))
				window.close();

			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::A)) {
				rotate = !rotate;
			}

			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Left)) {
				angleX = angleX - 0.1f;
			}

			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Right)) {
				angleX = angleX + 0.1f;
			}


			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Up)) {
				angleY = angleY + 0.1f;
			}

			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Down)) {
				angleY = angleY - 0.1f;
			}

			if (event.type == sf::Event::MouseWheelMoved)
			{
				z = z + (event.mouseWheel.delta*0.1f);
			}
		}

		//Prepare for drawing
		// Clear color and depth buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear z-buffer and set previously selected colour

		// Apply some transformations for the cube
		// The GL_MODELVIEW is used for transforming our model
		glMatrixMode(GL_MODELVIEW);
		glLightfv(GL_LIGHT0, GL_POSITION, LIGHT_POSITION);
		glLoadIdentity(); // reset

		glTranslatef(0.f, 0.f, z); // position the cube model at z-position -200; ie. away from us

		// rotate x,y,z by a given angle in degrees
		glRotatef(angleX * 20, 0.0f, 1.0f, 0.f); // rotate around the y-axis
		glRotatef(angleY * 20, 1.0f, 0.0f, 0.f); // rotate around the y-axis

		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glEnable(GL_CULL_FACE); // enable back face culling so we don't see the hidden vertices / textures of the cube
		glCullFace(GL_BACK);
		glColor3f(1, 0, 0);
		mesh.draw();

		glEnable(GL_DEPTH_TEST);
		// Finally, display rendered frame on screen
		window.display();
	}

	return EXIT_SUCCESS;
}