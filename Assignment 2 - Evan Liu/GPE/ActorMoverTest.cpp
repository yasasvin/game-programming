#include "gtest/gtest.h"
#include "ActorMover.h"

//==============================================================================
// MockActorMoverHandler
//==============================================================================
class MockActorMoverHandler : public IActorMoverHandler {

public:
    MockActorMoverHandler(CModel *a_pModel) :
            m_pModel(a_pModel), m_bIsCalled(false), m_fRemainder(0) {
    };

    virtual ~MockActorMoverHandler() {
    }

    virtual int GetTargetRow() {
        return m_iTargetRow;
    };

    virtual void SetTargetRow(int a_iRow) {
        m_iTargetRow = a_iRow;
    };

    virtual int GetTargetColumn() {
        return m_iTargetColumn;
    };

    virtual void SetTargetColumn(int a_iColumn) {
        m_iTargetColumn = a_iColumn;
    };

    virtual void OnMoveComplete(CActorMover *mover, float remainder) override;
    virtual bool CanMoveToTile(int a_iRow, int a_iColumn) override;

    bool m_bIsCalled;
    float m_fRemainder;

private:
    CModel *m_pModel;

    int m_iTargetRow;
    int m_iTargetColumn;
};

void MockActorMoverHandler::OnMoveComplete(CActorMover *mover, float remainder) {
    m_bIsCalled = true;
    m_fRemainder = remainder;
}
bool MockActorMoverHandler::CanMoveToTile(int a_iRow, int a_iColumn) {
    switch (m_pModel->GetTileValue(a_iRow, a_iColumn)) {
        case TileEmpty:
        case TileDot:
        case TileEnergizer:
            return true;
        default:
            return false;
    }
}

//==============================================================================
// ActorMoverTest
//==============================================================================
class ActorMoverTest : public ::testing::Test {

protected:

    CModel *model;
    CPosition *position;
    MockActorMoverHandler *handler;
    CActorMover *mover;

    virtual void SetUp() {
        model = new CModel();
        model->ReadMap(DEFAULT_MAP);

        position = new CPosition();
        handler = new MockActorMoverHandler(model);
    };
    virtual void TearDown() {
        delete mover;
        delete handler;
        delete position;
        delete model;
    };
};

TEST_F(ActorMoverTest, test_left) {
    mover = new CActorLeftMover(position, handler, model);
    EXPECT_EQ(DirectionLeft, mover->GetDirection());
    EXPECT_EQ(DirectionRight, mover->GetOpposite());

    position->SetX(100);
    mover->SetDestination(50);

    mover->MoveBy(30);
    EXPECT_EQ(70, position->GetX());
    EXPECT_FALSE(handler->m_bIsCalled);

    mover->MoveBy(30);
    EXPECT_EQ(50, position->GetX());
    EXPECT_TRUE(handler->m_bIsCalled);
    EXPECT_EQ(10, handler->m_fRemainder);

    EXPECT_FALSE(mover->CanMove(1, 1));
    EXPECT_TRUE(mover->CanMove(1, 2));

    //-- FindDestination
    handler->SetTargetRow(1);
    handler->SetTargetColumn(2);
    mover->FindDestination();
    EXPECT_EQ(1, handler->GetTargetColumn());
    EXPECT_EQ(model->GetCenterXOfColumn(1), mover->GetDestination());

    handler->SetTargetColumn(8);
    mover->FindDestination();
    EXPECT_EQ(6, handler->GetTargetColumn());
    EXPECT_EQ(model->GetCenterXOfColumn(6), mover->GetDestination());
}

TEST_F(ActorMoverTest, test_right) {
    mover = new CActorRightMover(position, handler, model);
    EXPECT_EQ(DirectionRight, mover->GetDirection());
    EXPECT_EQ(DirectionLeft, mover->GetOpposite());

    position->SetX(100);
    mover->SetDestination(150);

    mover->MoveBy(30);
    EXPECT_EQ(130, position->GetX());
    EXPECT_FALSE(handler->m_bIsCalled);

    mover->MoveBy(30);
    EXPECT_EQ(150, position->GetX());
    EXPECT_TRUE(handler->m_bIsCalled);
    EXPECT_EQ(10, handler->m_fRemainder);

    EXPECT_FALSE(mover->CanMove(2, 1));
    EXPECT_TRUE(mover->CanMove(1, 1));

    //-- FindDestination
    handler->SetTargetRow(1);
    handler->SetTargetColumn(2);
    mover->FindDestination();
    EXPECT_EQ(6, handler->GetTargetColumn());
    EXPECT_EQ(model->GetCenterXOfColumn(6), mover->GetDestination());

    handler->SetTargetColumn(8);
    mover->FindDestination();
    EXPECT_EQ(12, handler->GetTargetColumn());
    EXPECT_EQ(model->GetCenterXOfColumn(12), mover->GetDestination());
}

TEST_F(ActorMoverTest, test_up) {
    mover = new CActorUpMover(position, handler, model);
    EXPECT_EQ(DirectionUp, mover->GetDirection());
    EXPECT_EQ(DirectionDown, mover->GetOpposite());

    position->SetY(100);
    mover->SetDestination(50);

    mover->MoveBy(30);
    EXPECT_EQ(70, position->GetY());
    EXPECT_FALSE(handler->m_bIsCalled);

    mover->MoveBy(30);
    EXPECT_EQ(50, position->GetY());
    EXPECT_TRUE(handler->m_bIsCalled);
    EXPECT_EQ(10, handler->m_fRemainder);

    EXPECT_FALSE(mover->CanMove(1, 1));
    EXPECT_TRUE(mover->CanMove(2, 1));

    //-- FindDestination
    handler->SetTargetColumn(6);
    handler->SetTargetRow(3);
    mover->FindDestination();
    EXPECT_EQ(1, handler->GetTargetRow());
    EXPECT_EQ(model->GetCenterYOfRow(1), mover->GetDestination());

    handler->SetTargetRow(7);
    mover->FindDestination();
    EXPECT_EQ(5, handler->GetTargetRow());
    EXPECT_EQ(model->GetCenterYOfRow(5), mover->GetDestination());
}

TEST_F(ActorMoverTest, test_down) {
    mover = new CActorDownMover(position, handler, model);
    EXPECT_EQ(DirectionDown, mover->GetDirection());
    EXPECT_EQ(DirectionUp, mover->GetOpposite());

    position->SetY(100);
    mover->SetDestination(150);

    mover->MoveBy(30);
    EXPECT_EQ(130, position->GetY());
    EXPECT_FALSE(handler->m_bIsCalled);

    mover->MoveBy(30);
    EXPECT_EQ(150, position->GetY());
    EXPECT_TRUE(handler->m_bIsCalled);
    EXPECT_EQ(10, handler->m_fRemainder);

    EXPECT_FALSE(mover->CanMove(1, 2));
    EXPECT_TRUE(mover->CanMove(1, 1));

    //-- FindDestination
    handler->SetTargetColumn(6);
    handler->SetTargetRow(3);
    mover->FindDestination();
    EXPECT_EQ(5, handler->GetTargetRow());
    EXPECT_EQ(model->GetCenterYOfRow(5), mover->GetDestination());

    handler->SetTargetRow(10);
    mover->FindDestination();
    EXPECT_EQ(14, handler->GetTargetRow());
    EXPECT_EQ(model->GetCenterYOfRow(14), mover->GetDestination());
}

TEST_F(ActorMoverTest, test_cross_screen_left) {
    mover = new CActorLeftMover(position, handler, model);

    handler->SetTargetRow(14);

    handler->SetTargetColumn(1);
    EXPECT_FALSE(mover->CanCrossScreen());

    handler->SetTargetColumn(0);
    EXPECT_TRUE(mover->CanCrossScreen());

    mover->CrossScreen(1.0f);
    EXPECT_EQ(model->GetCenterXOfColumn(COLUMN_COUNT - 1) - 1, position->GetX());
    EXPECT_EQ(21, handler->GetTargetColumn());
    EXPECT_EQ(model->GetCenterXOfColumn(21), mover->GetDestination());

    mover->MoveBy(TILE_SIZE * 10);
    EXPECT_TRUE(handler->m_bIsCalled);
    EXPECT_EQ(model->GetCenterXOfColumn(21), mover->GetDestination());
    EXPECT_EQ(position->GetX(), mover->GetDestination());
}

TEST_F(ActorMoverTest, test_cross_screen_right) {
    mover = new CActorRightMover(position, handler, model);

    handler->SetTargetRow(14);

    handler->SetTargetColumn(26);
    EXPECT_FALSE(mover->CanCrossScreen());

    handler->SetTargetColumn(27);
    EXPECT_TRUE(mover->CanCrossScreen());

    mover->CrossScreen(1.0f);
    EXPECT_EQ(model->GetCenterXOfColumn(0) + 1, position->GetX());
    EXPECT_EQ(6, handler->GetTargetColumn());
    EXPECT_EQ(model->GetCenterXOfColumn(6), mover->GetDestination());
}