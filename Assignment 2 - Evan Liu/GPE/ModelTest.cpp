#include "gtest/gtest.h"
#include "Model.h"

class ModelTest : public ::testing::Test {

protected:

    CModel *model;
    int tileSize;

    virtual void SetUp() {
        model = new CModel();
        tileSize = model->GetTileSize();
    }

    virtual void TearDown() {
        delete model;
    }
};

TEST_F(ModelTest, test_init) {

    //-- Size --//
    EXPECT_EQ(TILE_SIZE, tileSize);

    int rows = model->GetRows();
    int columns = model->GetColumns();
    EXPECT_EQ(ROW_COUNT, rows);
    EXPECT_EQ(COLUMN_COUNT, columns);

    int width = model->GetWidth();
    int height = model->GetHeight();
    EXPECT_EQ(tileSize * rows, width);
    EXPECT_EQ(tileSize * columns, height);

    //-- State --//

    EGameState state = model->GetGameState();
    EXPECT_EQ(GameStateNone, state);

    bool running = model->GetRunning();
    EXPECT_FALSE(running);

    bool gameOver = model->GetGameOver();
    EXPECT_FALSE(gameOver);

    bool isWin = model->GetWin();
    bool isLose = model->GetLose();
    EXPECT_FALSE(isWin);
    EXPECT_FALSE(isLose);

    int score = model->GetScore();
    EXPECT_EQ(0, score);
    
    //-- Actor --//
    EGhostState const &redState = model->GetRedData()->GetState();
    EXPECT_EQ(GhostStateIdle, redState);
}

TEST_F(ModelTest, test_map_position) {
    CPosition *mapPosition = model->GetMapPosition();
    EXPECT_EQ(0, mapPosition->GetX());
    EXPECT_EQ(0, mapPosition->GetY());

    model->SetMapPosition(100.1f, 200.2f);
    EXPECT_EQ(100.1f, mapPosition->GetX());
    EXPECT_EQ(200.2f, mapPosition->GetY());
}

TEST_F(ModelTest, test_read_map) {
    model->ReadMap(DEFAULT_MAP);

    EXPECT_EQ(TileWall, model->GetTileValue(0, 0));
    EXPECT_EQ(TileDot, model->GetTileValue(1, 1));

    int pacmanRow = 23;
    int pacmanColumn = 14;
    EXPECT_EQ(TileEmpty, model->GetTileValue(pacmanRow, pacmanColumn - 1));
    EXPECT_EQ(TileEmpty, model->GetTileValue(pacmanRow, pacmanColumn));

    float pacmanX = model->GetXOfColumn(pacmanColumn);
    float pacmanY = model->GetCenterYOfRow(pacmanRow);

    EXPECT_EQ(pacmanX, model->GetPacmanData()->GetOrigin()->GetX());
    EXPECT_EQ(pacmanY, model->GetPacmanData()->GetOrigin()->GetY());
    EXPECT_EQ(pacmanX, model->GetPacmanData()->GetPosition()->GetX());
    EXPECT_EQ(pacmanY, model->GetPacmanData()->GetPosition()->GetY());
}

TEST_F(ModelTest, test_controller_direction) {
    EXPECT_EQ(DirectionNone, model->GetControllerDirection());

    model->SetControllerDirection(DirectionUp);
    EXPECT_EQ(DirectionUp, model->GetControllerDirection());

    model->SetControllerDirection(DirectionNone);
    EXPECT_EQ(DirectionNone, model->GetControllerDirection());
}

TEST_F(ModelTest, test_cell_coordinates) {
    model->GetMapPosition()->Reset(100, 100);

    EXPECT_EQ(100, model->GetXOfColumn(0));
    EXPECT_EQ(100, model->GetYOfRow(0));

    EXPECT_EQ(100 + tileSize, model->GetXOfColumn(1));
    EXPECT_EQ(100 + tileSize, model->GetYOfRow(1));

    EXPECT_EQ(100 + tileSize * 2, model->GetXOfColumn(2));
    EXPECT_EQ(100 + tileSize * 2, model->GetYOfRow(2));
}

TEST_F(ModelTest, test_cell_center_coordinates) {
    model->GetMapPosition()->Reset(100, 100);

    EXPECT_EQ(100 + tileSize / 2, model->GetCenterXOfColumn(0));
    EXPECT_EQ(100 + tileSize / 2, model->GetCenterYOfRow(0));

    EXPECT_EQ(100 + tileSize * 1.5, model->GetCenterXOfColumn(1));
    EXPECT_EQ(100 + tileSize * 1.5, model->GetCenterYOfRow(1));

    EXPECT_EQ(100 + tileSize * 2.5, model->GetCenterXOfColumn(2));
    EXPECT_EQ(100 + tileSize * 2.5, model->GetCenterYOfRow(2));
}

TEST_F(ModelTest, test_cell_from_coordinates) {
    model->GetMapPosition()->Reset(100, 100);

    EXPECT_EQ(0, model->GetColumnFromX(100));
    EXPECT_EQ(0, model->GetRowFromY(100));

    EXPECT_EQ(1, model->GetColumnFromX(100 + tileSize));
    EXPECT_EQ(1, model->GetRowFromY(100 + tileSize));

    EXPECT_EQ(1, model->GetColumnFromX(100 + tileSize * 1.9));
    EXPECT_EQ(1, model->GetRowFromY(100 + tileSize * 1.9));

    EXPECT_EQ(2, model->GetColumnFromX(100 + tileSize * 2));
    EXPECT_EQ(2, model->GetRowFromY(100 + tileSize * 2));
}