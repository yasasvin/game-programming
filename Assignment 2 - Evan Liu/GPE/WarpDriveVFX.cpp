#include "StdAfx.h"
#include "Game.h"
#include "resource.h"
#include "Level.h"
#include "AnimatedSprite.h"
#include "WarpDriveVFX.h"
#include "BackBuffer.h"
#include "BoundingRect.h"


CWarpDriveVFX::CWarpDriveVFX(void)
{
}


CWarpDriveVFX::~CWarpDriveVFX(void)
{
}

void CWarpDriveVFX::Initialise(float a_fWarpSpeed)
{
	m_fWarpSpeed = a_fWarpSpeed;
	m_AnimationAngle = 0;
}

void CWarpDriveVFX::Render()
{
}

void CWarpDriveVFX::Process(float a_fDeltaTick)
{
}

CBoundingRect CWarpDriveVFX::GetBoundingRectForNextFrame(float a_fDeltaTick)
{
	CBoundingRect a; 
	a.x1 = 74;
	a.y1 = 212; 
	a.x2 = 74;
	a.y2 = 212;
	return a; 
}

CBoundingRect CWarpDriveVFX::GetBoundingRect2ForNextFrame(float a_fDeltaTick)
{
	CBoundingRect b;
	b.x1 = 328;
	b.y1 = 212;
	b.x2 = 328;
	b.y2 = 212;
	return b;
}