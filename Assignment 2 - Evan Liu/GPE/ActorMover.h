#pragma once

#include "Model.h"

class CActorMover;

//==============================================================================
// IActorMoverHandler
//==============================================================================
class IActorMoverHandler {

public:
    virtual int GetTargetRow() = 0;
    virtual void SetTargetRow(int a_iRow) = 0;

    virtual int GetTargetColumn() = 0;
    virtual void SetTargetColumn(int a_iColumn) = 0;

    virtual void OnMoveComplete(CActorMover *mover, float remainder) = 0;

    virtual bool CanMoveToTile(int a_iRow, int a_iColumn) = 0;
};

//==============================================================================
// CActorMover
//==============================================================================
class CActorMover {

public:
    CActorMover(EDirection a_eDirection, CPosition *a_pPosition, IActorMoverHandler *a_pHandler, CModel *a_pModel);

    virtual ~CActorMover();

    EDirection const &GetDirection() const;
    EDirection const &GetOpposite() const;

    float GetDestination() const;

    void SetDestination(float a_fDestination);

    virtual void MoveBy(float a_fValue) = 0;

    virtual bool CanMove(int a_iRow, int a_iColumn) = 0;

    virtual void FindDestination() = 0;

    virtual bool CanMove();

    virtual bool CanCrossScreen();

    virtual void CrossScreen(float a_fMoveValue);

protected:
    EDirection m_eDirection;
    EDirection m_eOpposite;

    IActorMoverHandler *m_pHandler;
    CPosition *m_pPosition;
    CModel *m_pModel;

    float m_fDestination;

    void Complete(float a_fRemainder);

};

//==============================================================================
// CActorLeftMover
//==============================================================================
class CActorLeftMover : public CActorMover {

public:
    CActorLeftMover(CPosition *a_pPosition, IActorMoverHandler *a_pHandler, CModel *a_pModel) :
            CActorMover(DirectionLeft, a_pPosition, a_pHandler, a_pModel) {
    }

    virtual void MoveBy(float a_fValue) override;

    virtual bool CanMove(int a_iRow, int a_iColumn) override;

    virtual void FindDestination() override;

    virtual bool CanCrossScreen() override;

    virtual void CrossScreen(float a_fMoveValue) override;
};

//==============================================================================
// CActorRightMover
//==============================================================================
class CActorRightMover : public CActorMover {

public:
    CActorRightMover(CPosition *a_pPosition, IActorMoverHandler *a_pHandler, CModel *a_pModel) :
            CActorMover(DirectionRight, a_pPosition, a_pHandler, a_pModel) {
    }

    virtual void MoveBy(float a_fValue) override;

    virtual bool CanMove(int a_iRow, int a_iColumn) override;

    virtual void FindDestination() override;

    virtual bool CanCrossScreen() override;

    virtual void CrossScreen(float a_fMoveValue) override;
};

//==============================================================================
// CActorUpMover
//==============================================================================
class CActorUpMover : public CActorMover {

public:
    CActorUpMover(CPosition *a_pPosition, IActorMoverHandler *a_pHandler, CModel *a_pModel) :
            CActorMover(DirectionUp, a_pPosition, a_pHandler, a_pModel) {
    }

    virtual void MoveBy(float a_fValue) override;

    virtual bool CanMove(int a_iRow, int a_iColumn) override;

    virtual void FindDestination() override;
};

//==============================================================================
// CActorDownMover
//==============================================================================
class CActorDownMover : public CActorMover {

public:
    CActorDownMover(CPosition *a_pPosition, IActorMoverHandler *a_pHandler, CModel *a_pModel) :
            CActorMover(DirectionDown, a_pPosition, a_pHandler, a_pModel) {
    }

    virtual void MoveBy(float a_fValue) override;

    virtual bool CanMove(int a_iRow, int a_iColumn) override;

    virtual void FindDestination() override;
};