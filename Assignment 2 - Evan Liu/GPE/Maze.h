#pragma once
#include <vector>
#include "WinDef.h"
#include "Sprite.h"


class CMaze : public CSprite
{
public:
	CMaze(void);
	~CMaze(void);

	void Initialise(int a_iWidth, int a_iHeight, int a_iBlockSize);
	void InitialiseMap();
	void InitialisePill(int iPillSize);
	void Process(float a_fDeltaTick);
	void Render();
	void RenderPill(int iX, int iY);
	std::vector<RECT>* GetPointerToMazeData() { return &m_Maze; }


private:
	std::vector<RECT> m_Maze;  
};



