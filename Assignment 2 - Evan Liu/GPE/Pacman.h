#pragma once
#include "BoundingRect.h"

class CAnimatedSprite;
class CActorData;
class CPosition;
class CPacman
{
public:
	enum EFacing
	{
		INVALID_FACING = -1,
		FACING_FORWARD,
		FACING_LEFT,
		FACING_RIGHT,
		FACING_BACKWARD,
		MAX_FACING
	};
public:
	CPacman();
	virtual ~CPacman(void);

	virtual bool Deinitialise();
	virtual bool Initialise(CActorData *m_pData);

	virtual void Render();
	void checkFacing();
	virtual void Process(float a_fDeltaTick);
	void warpRight();
	void warpLeft();
	void setStopWalkingState();
	void setWalkState(EFacing a_eDirection);
	void setStopWalkingState(EFacing a_eDirection);

	CBoundingRect GetBoundingRectForNextFrame(float a_fDeltaTick);
	
private:
	void walk();
	bool					m_bWalking[MAX_FACING];
	CAnimatedSprite			*m_pAnim[MAX_FACING];
	EFacing					m_eFacing;
	EFacing					m_eDirection;
	CActorData				*m_pData;
	CPosition				*m_pPosition;
	CBoundingRect			m_BoundingRect;

};

