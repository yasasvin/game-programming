//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by GPE.rc
//
#define IDC_MYICON                      2
#define IDD_GPE_DIALOG                  102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_GPE                         107
#define IDI_SMALL                       108
#define IDC_GPE                         109
#define IDR_MAINFRAME                   128
#define IDB_BOMBERBOYBLACK              129
#define IDB_BOMBERBOYMASK               130
#define IDB_BOMBERBOYWHITE              131
#define IDR_BEAM                        132
#define IDR_ENERGIZE                    133
#define IDR_TELEPORT                    134
#define IDR_WARP                        135
#define IDB_BITMAPBOMB                  137
#define IDB_PACMAN						140
#define IDB_PACMANMASK					141
#define IDB_GHOSTCYAN					142
#define IDB_GHOSTRED					143
#define IDB_GHOSTPINK					144
#define	IDB_GHOSTORANGE					145
#define IDB_GHOSTMASK					146
#define IDB_MAP							150
#define IDB_MAPMASK						151
#define IDB_SMALLPILL					152
#define IDB_BIGPILL						153
#define IDB_BIGPILLMASK					154
#define IDB_SMALLPILLMASK				155
#define IDR_PACMANBEGIN					156
#define IDR_PACMANCHOMP					157
#define IDR_PACMANEATGHOST				158
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
