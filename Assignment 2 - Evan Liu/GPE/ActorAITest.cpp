#include "gtest/gtest.h"
#include "ActorAI.h"

//==============================================================================
// PacmanAITest
//==============================================================================
class PacmanAITest : public ::testing::Test {

protected:
    CPacmanAI *pacmanAI;
    CModel *model;
    CActorData *pacmanData;
    CPosition *pacmanOrigin;
    CPosition *pacmanPosition;

    virtual void SetUp() {
        model = new CModel();
        model->ReadMap(DEFAULT_MAP);

        pacmanData = model->GetPacmanData();
        pacmanOrigin = pacmanData->GetOrigin();
        pacmanPosition = pacmanData->GetPosition();

        pacmanAI = model->m_pPacmanAI;
    }

    virtual void TearDown() {
        delete pacmanAI;
        delete model;
    }

};

TEST_F(PacmanAITest, test_init) {
    EXPECT_EQ(DirectionNone, pacmanData->GetDirection());
    EXPECT_EQ(pacmanPosition->GetX(), pacmanOrigin->GetX());
    EXPECT_EQ(pacmanPosition->GetY(), pacmanOrigin->GetY());
}

TEST_F(PacmanAITest, test_non_move) {
    model->Process(1);
    EXPECT_EQ(DirectionNone, pacmanData->GetDirection());
    EXPECT_EQ(pacmanPosition->GetX(), pacmanOrigin->GetX());
    EXPECT_EQ(pacmanPosition->GetY(), pacmanOrigin->GetY());
}

TEST_F(PacmanAITest, test_start_move_left) {
    float deltaTick = (float) TILE_SIZE / PACMAN_SPEED / 2;
    float oldX = pacmanPosition->GetX();

    model->SetControllerDirection(DirectionLeft);
    model->Process(deltaTick);

    EXPECT_EQ(DirectionLeft, pacmanData->GetDirection());
    EXPECT_EQ(oldX - TILE_SIZE / 2, pacmanPosition->GetX());
}

TEST_F(PacmanAITest, test_start_move_left_and_stop) {
    pacmanAI->SetTargetRow(3);
    pacmanAI->SetTargetColumn(1);
    pacmanPosition->Reset(model->GetCenterXOfColumn(1), model->GetCenterYOfRow(3));

    model->SetControllerDirection(DirectionLeft);
    model->Process(10);

    EXPECT_EQ(model->GetCenterXOfColumn(1), pacmanPosition->GetX());
}

TEST_F(PacmanAITest, test_start_move_left_fail) {
    pacmanAI->SetTargetRow(1);
    pacmanAI->SetTargetColumn(15);
    pacmanPosition->Reset(model->GetCenterXOfColumn(15), model->GetCenterYOfRow(1));

    model->SetControllerDirection(DirectionLeft);
    model->Process(10);

    EXPECT_EQ(DirectionNone, pacmanData->GetDirection());
    EXPECT_EQ(15, pacmanAI->GetTargetColumn());
    EXPECT_EQ(model->GetCenterXOfColumn(15), pacmanPosition->GetX());
}

TEST_F(PacmanAITest, test_start_move_left_and_turn) {
    pacmanAI->SetTargetRow(5);
    pacmanAI->SetTargetColumn(3);
    pacmanPosition->Reset(model->GetCenterXOfColumn(3), model->GetCenterYOfRow(5));

    model->SetControllerDirection(DirectionLeft);
    model->Process(0.1);

    model->SetControllerDirection(DirectionUp);
    model->Process(10);

    EXPECT_EQ(model->GetCenterXOfColumn(1), pacmanPosition->GetX());
    EXPECT_TRUE(pacmanPosition->GetY() < model->GetCenterYOfRow(5));
}

TEST_F(PacmanAITest, test_cross_screen_left) {
    pacmanPosition->Reset(model->GetCenterXOfColumn(1), model->GetCenterYOfRow(14));
    pacmanAI->SetTargetRow(14);
    pacmanAI->SetTargetColumn(1);

    model->SetControllerDirection(DirectionLeft);
    float deltaTick = (float) (TILE_SIZE + 1) / PACMAN_SPEED;
    model->Process(deltaTick);

    EXPECT_EQ(model->GetCenterXOfColumn(COLUMN_COUNT - 1) - 1, pacmanPosition->GetX());
    EXPECT_EQ(21, pacmanAI->GetTargetColumn());
}

TEST_F(PacmanAITest, test_cross_screen_right) {
    pacmanPosition->Reset(model->GetCenterXOfColumn(COLUMN_COUNT - 2), model->GetCenterYOfRow(14));
    pacmanAI->SetTargetRow(14);
    pacmanAI->SetTargetColumn(COLUMN_COUNT - 2);

    model->SetControllerDirection(DirectionRight);
    float deltaTick = (float) (TILE_SIZE + 1) / PACMAN_SPEED;
    model->Process(deltaTick);

    EXPECT_EQ(model->GetCenterXOfColumn(0) + 1, pacmanPosition->GetX());
    EXPECT_EQ(6, pacmanAI->GetTargetColumn());
}