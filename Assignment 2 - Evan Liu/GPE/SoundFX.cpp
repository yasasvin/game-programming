#include "StdAfx.h"
#include "SoundFX.h"
#include "resource.h"

CSoundFX::CSoundFX(HINSTANCE a_hInstance) : 
	m_hInstance(a_hInstance), 
	m_bPlaying(false),
	m_fSoundRepeat(2.3),
	m_fTimeElapsed(0.0f),
	m_eSoundFXPlay(NOSOUND)
{
}

CSoundFX::~CSoundFX(void)
{
}

void CSoundFX::SetSoundFXState(ESoundFXPlay a_eSoundFXPlay)
{
	m_eSoundFXPlay = a_eSoundFXPlay; 
}

void CSoundFX::Play()
{
	if(m_eSoundFXPlay == NOSOUND)
	{
		m_bPlaying = false; 
		m_fTimeElapsed = 0.0;
	}
	else
	{
		if(!m_bPlaying)
		{
			switch(m_eSoundFXPlay)
			{
				case (BEAMSOUND) : 
						PlayBeamSound(); 
						m_bPlaying = true; 
						m_fTimeElapsed = 0.0;
						break; 
				case (TELEPORTSOUND) : 
						PlayTeleportSound(); 
						m_fTimeElapsed = 0.0;
						m_bPlaying = true; 
						break; 
				case (ENERGIZESOUND) :
						PlayEnergizeSound();
						m_fTimeElapsed = 0.0;
						m_bPlaying = true; 
						break; 
				case (WARPSOUND) :
						PlayWarpSound(); 
						m_bPlaying = true; 
						m_fTimeElapsed = 0.0;
						break; 
				case (PACMANBEGIN) :
						PlayBeginGame();
						m_bPlaying = true;
						m_fTimeElapsed = 0.0;
						break;
				case (PACMANCHOMP):
						PlayPacmanChomp();
						m_bPlaying = true;
						m_fTimeElapsed = 0.0;
						break;
				default:
					break; 
			}
		}
	}
}

void CSoundFX::Process(float a_fDeltaTick)
{
	m_fTimeElapsed += a_fDeltaTick;

	if (m_fTimeElapsed >= m_fSoundRepeat && m_bPlaying)
	{
		m_fTimeElapsed = 0;
		m_bPlaying = false; 
	}
	
}

void CSoundFX::PlayTeleportSound()
{
	PlaySound(MAKEINTRESOURCE(IDR_TELEPORT), m_hInstance, SND_RESOURCE | SND_ASYNC);
}

void CSoundFX::PlayWarpSound()
{
	PlaySound(MAKEINTRESOURCE(IDR_WARP), m_hInstance, SND_RESOURCE | SND_ASYNC);
}

void CSoundFX::PlayEnergizeSound()
{
	PlaySound(MAKEINTRESOURCE(IDR_ENERGIZE), m_hInstance, SND_RESOURCE | SND_ASYNC);
}

void CSoundFX::PlayBeamSound()
{
	PlaySound(MAKEINTRESOURCE(IDR_BEAM), m_hInstance, SND_RESOURCE | SND_ASYNC);
}

void CSoundFX::PlayBeginGame()
{
	PlaySound(MAKEINTRESOURCE(IDR_PACMANBEGIN), m_hInstance, SND_RESOURCE | SND_NOSTOP);
}

void CSoundFX::PlayPacmanChomp()
{
	PlaySound(MAKEINTRESOURCE(IDR_PACMANCHOMP), m_hInstance, SND_RESOURCE | SND_ASYNC);
}