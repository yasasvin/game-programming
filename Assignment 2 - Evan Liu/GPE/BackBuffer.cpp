#include "StdAfx.h"
#include "BackBuffer.h"
#include "Model.h"
#include "Game.h"
#include <sstream> 
#include <string>

CBackBuffer::CBackBuffer() :
		m_hWnd(0),
		m_hDC(0),
		m_hSurface(0),
		m_hOldObject(0),
		m_iWidth(0),
		m_iHeight(0)
{
}

CBackBuffer::~CBackBuffer()
{
	SelectObject(m_hDC, m_hOldObject);
	DeleteObject(m_hSurface);
	DeleteObject(m_hDC);
}

bool CBackBuffer::Initialise(HWND a_hWnd, int a_iWidth, int a_iHeight)
{
	m_hWnd			= a_hWnd;
	m_iWidth		= a_iWidth;
	m_iHeight		= a_iHeight;

	HDC hWindowDC	= GetDC(m_hWnd);
	m_hDC			= CreateCompatibleDC(hWindowDC);
	m_hSurface		= CreateCompatibleBitmap(hWindowDC, m_iWidth, m_iHeight);
	ReleaseDC(m_hWnd, hWindowDC);

	m_hOldObject		= static_cast<HBITMAP>(SelectObject(m_hDC, m_hSurface));
	HBRUSH brushBlack	= static_cast<HBRUSH>(GetStockObject(BLACK_BRUSH));
	HBRUSH oldBrush		= static_cast<HBRUSH>(SelectObject(m_hDC, brushBlack));

	Rectangle(m_hDC, 0, 0, m_iWidth, m_iHeight);
	SelectObject(m_hDC, oldBrush);
	return (true);
}

void CBackBuffer::Clear()
{
	HBRUSH hOldBrush = static_cast<HBRUSH>(SelectObject(GetBFDC(), GetStockObject(BLACK_BRUSH)));
	Rectangle(GetBFDC(), 0, 0, GetWidth(), GetHeight());
	SelectObject(GetBFDC(), hOldBrush);
}

HDC CBackBuffer::GetBFDC() const
{
	return (m_hDC);
}

int CBackBuffer::GetWidth() const
{
	return (m_iWidth);
}

int CBackBuffer::GetHeight() const
{
	return (m_iHeight);
}

void CBackBuffer::Present()
{
	HDC hWndDC = GetDC(m_hWnd);
	BitBlt(hWndDC, 0, 0, m_iWidth, m_iHeight, m_hDC, 0, 0, SRCCOPY);
	m_pModel = CGame::GetInstance().GetModel();
	SetTextColor(hWndDC, COLORREF(RGB(255, 255, 255)));
	SetBkColor(hWndDC, COLORREF(RGB(0, 0, 0)));

	std::wstring score = std::to_wstring((long long)m_pModel->GetScore());
	std::wstring pacmanLife = std::to_wstring((long long)m_pModel->GetPacmanLife());
	TextOut(hWndDC, 10, 10, TEXT("Score: "), 7);
	TextOut(hWndDC, 80, 10, score.c_str(), _tcslen(score.c_str()));
	if (m_pModel->GetPacmanLife() >= 0)
	{
		TextOut(hWndDC, 10, 30, TEXT("Life: "), 6);
		TextOut(hWndDC, 80, 30, pacmanLife.c_str(), _tcslen(pacmanLife.c_str()));
	}
	else
	{
		TextOut(hWndDC, 10, 30, TEXT("Life: 0"), 7);
		TextOut(hWndDC, 120, 348, TEXT("Press Enter to Restart!"), 23);
	}
	if (m_pModel->GetWin())
	{
		TextOut(hWndDC, 10, 50, TEXT("You Win"), 7);
		TextOut(hWndDC, 10, 70, TEXT("Game Over"), 9);
	}
	else if (m_pModel->GetLose())
	{
		TextOut(hWndDC, 10, 50, TEXT("You Lose"), 8);
		TextOut(hWndDC, 10, 70, TEXT("Game Over"), 9);
	}
	ReleaseDC(m_hWnd, hWndDC);
}
