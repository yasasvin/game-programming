#include "stdafx.h"
#include "Level.h"
#include "Game.h"
#include "BomberBoy.h"
#include "BackGroundPlane.h"
#include "WarpDriveVFX.h"
#include "BoundingRect.h"
#include "SoundFX.h"
#include "FractalVFX.h"
#include "Maze.h"
#include "Model.h"
#include "Pacman.h"
#include "Ghost.h"

CLevel::CLevel()
{
}

CLevel::~CLevel()
{
	delete m_pBackGroundPlane;
	m_pBackGroundPlane = NULL;

	delete m_pMiddleGroundPlane;
	m_pMiddleGroundPlane = NULL;

	delete m_pWarpDriveVFX;
	m_pWarpDriveVFX = NULL; 

	delete m_pFractalVFX;
	m_pFractalVFX = NULL;

	delete m_pMaze;
	m_pMaze = NULL;

	delete ghostData;
	ghostData = NULL;

	delete m_pModel;
	m_pModel = NULL;

	delete ghostPosition;
	ghostPosition = NULL;

	Deinitialise();
}

bool CLevel::Deinitialise()
{
	return (true);
}

bool CLevel::Initialise(int a_iWidth, int a_iHeight)
{
	m_iWidth		= a_iWidth;
	m_iHeight		= a_iHeight;
	m_timePassedSinceStartOfGame = 0;

	m_pModel		= CGame::GetInstance().GetModel();
	ghostData		= m_pModel->GetRedData();
	ghostPosition	= ghostData->GetPosition();

	m_pPacman = new CPacman();
	m_pPacman->Initialise(m_pModel->GetPacmanData());

	m_pGhostRed = new CGhost();
	m_pGhostRed->Initialise(m_pModel->GetRedData(), CGhost::GHOSTCOLOURRED);

	m_pGhostPink = new CGhost();
	m_pGhostPink->Initialise(m_pModel->GetPinkData(), CGhost::GHOSTCOLOURPINK);

	m_pGhostCyan = new CGhost();
	m_pGhostCyan->Initialise(m_pModel->GetCyanData(), CGhost::GHOSTCOLOURCYAN);

	m_pGhostOrange = new CGhost();
	m_pGhostOrange->Initialise(m_pModel->GetOrangeData(), CGhost::GHOSTCOLOURORANGE);

	m_pBackGroundPlane = new CBackGroundPlane(); 
	m_pBackGroundPlane->Initialise(30.0f); 

	m_pMiddleGroundPlane = new CBackGroundPlane(); 
	m_pMiddleGroundPlane->Initialise(60.0f); 

	m_pWarpDriveVFX = new CWarpDriveVFX(); 
	m_pWarpDriveVFX->Initialise(1.0f); 

	m_pWarpDriveVFX2 = new CWarpDriveVFX();
	m_pWarpDriveVFX2->Initialise(1.0f);

	m_pFractalVFX = new CFractalVFX(); 

	m_pMaze = new CMaze();
	m_pMaze->Initialise(384,384,32); 
	m_pMaze->InitialiseMap();

	int smallPill = 0;
	int bigPill   = 1;
	m_pMazeSmallPill = new CMaze();
	m_pMazeSmallPill->InitialisePill(smallPill);

	m_pMazeBigPill = new CMaze();
	m_pMazeBigPill->InitialisePill(bigPill);
	CGame::GetInstance().GetSoundFXPlayer()->SetSoundFXState(CSoundFX::PACMANBEGIN);

	return (true);
}

void CLevel::RenderDotTo(int iX, int iY)
{
	m_pMazeSmallPill->RenderPill(iX, iY);
}

void CLevel::RenderEnergizerTo(int iX, int iY)
{
	m_pMazeBigPill->RenderPill(iX, iY);
}

void CLevel::Render()
{
	m_pMaze->Render(); 
	CModel *model = CGame::GetInstance().GetModel();
	for (int i = 0; i < model->GetRows(); ++i) {
		for (int j = 0; j < model->GetColumns(); ++j) {
			float x = model->GetCenterXOfColumn(j);
			float y = model->GetCenterYOfRow(i);

			ETileValue tileValue = model->GetTileValue(i, j);
			if (tileValue == TileDot) {
				RenderDotTo(x, y);
			}
			else if (tileValue == TileEnergizer) {
				RenderEnergizerTo(x, y);
			}
		}
	}
	m_pGhostRed->Render();
	m_pGhostPink->Render();
	m_pGhostCyan->Render();
	m_pGhostOrange->Render();
	m_pPacman->Render();
}


void CLevel::Process(float a_fDeltaTick)
{
	m_pPacman->Process(a_fDeltaTick);
	m_pGhostRed->Process(a_fDeltaTick);
	m_pGhostPink->Process(a_fDeltaTick);
	m_pGhostCyan->Process(a_fDeltaTick);
	m_pGhostOrange->Process(a_fDeltaTick);
	m_pBackGroundPlane->Process(a_fDeltaTick); 
	m_pMiddleGroundPlane->Process(a_fDeltaTick); 
	m_pWarpDriveVFX->Process(a_fDeltaTick); 
}

bool CLevel::ProcessEndGame()
{
//	CGame::GetInstance().SetGameOver(); 
	return true;
}

CBomberBoy* CLevel::GetBomberBoyBlack()
{
	return m_pBomberBoyBlack;
}

CBomberBoy* CLevel::GetBomberBoyWhite()
{
	return m_pBomberBoyWhite;
}
