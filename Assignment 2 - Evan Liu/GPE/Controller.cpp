#include "StdAfx.h"
#include "Controller.h"
#include "Level.h"
#include "BomberBoy.h"
#include "SoundFX.h"
#include "Game.h"
#include "Model.h"

CController::CController() 
{
}

CController::~CController(void)
{
}

void CController::ProcessKeyDownMessage(char a_wParam)
{
	switch (a_wParam)
	{
		case 'W':
		{
			//CGame::GetInstance().GetLevel()->GetBomberBoyWhite()->SetWalkState(CBomberBoy::FACING_BACKWARD);
			break;
		}
		case 'S':
		{
			//CGame::GetInstance().GetLevel()->GetBomberBoyWhite()->SetWalkState(CBomberBoy::FACING_FORWARD);
			break;
		}
		case 'D':
		{
			//CGame::GetInstance().GetLevel()->GetBomberBoyWhite()->SetWalkState(CBomberBoy::FACING_RIGHT);
			break;
		}
		case 'A':
		{
			//CGame::GetInstance().GetLevel()->GetBomberBoyWhite()->SetWalkState(CBomberBoy::FACING_LEFT);
			break;
		}
		case VK_UP:
		{
			CGame::GetInstance().GetModel()->SetControllerDirection(DirectionUp);
			//CGame::GetInstance().GetLevel()->GetBomberBoyBlack()->SetWalkState(CBomberBoy::FACING_BACKWARD);
			break;
		}
		case VK_DOWN:
		{
			CGame::GetInstance().GetModel()->SetControllerDirection(DirectionDown);
			//CGame::GetInstance().GetLevel()->GetBomberBoyBlack()->SetWalkState(CBomberBoy::FACING_FORWARD);
			break;
		}
		case VK_RIGHT:
		{
			CGame::GetInstance().GetModel()->SetControllerDirection(DirectionRight);
			//CGame::GetInstance().GetLevel()->GetBomberBoyBlack()->SetWalkState(CBomberBoy::FACING_RIGHT);
			break;
		}
		case VK_LEFT:
		{
			CGame::GetInstance().GetModel()->SetControllerDirection(DirectionLeft);
			//CGame::GetInstance().GetLevel()->GetBomberBoyBlack()->SetWalkState(CBomberBoy::FACING_LEFT);
			break;
		}
	}
}


void CController::ProcessKeyUpMessage(char a_wParam)
{
	switch (a_wParam)
	{
		case 'W':
		{
			//CGame::GetInstance().GetLevel()->GetBomberBoyWhite()->SetStopWalkingState(CBomberBoy::FACING_BACKWARD);
			//CGame::GetInstance().GetSoundFXPlayer()->SetSoundFXState(CSoundFX::NOSOUND);
			break;
		}
		case 'S':
		{
			//CGame::GetInstance().GetLevel()->GetBomberBoyWhite()->SetStopWalkingState(CBomberBoy::FACING_FORWARD);
			//CGame::GetInstance().GetSoundFXPlayer()->SetSoundFXState(CSoundFX::NOSOUND);
			break;
		}
		case 'D':
		{
			//CGame::GetInstance().GetLevel()->GetBomberBoyWhite()->SetStopWalkingState(CBomberBoy::FACING_RIGHT);
			//CGame::GetInstance().GetSoundFXPlayer()->SetSoundFXState(CSoundFX::NOSOUND);
			break;
		}
		case 'A':
		{
			//CGame::GetInstance().GetLevel()->GetBomberBoyWhite()->SetStopWalkingState(CBomberBoy::FACING_LEFT);
			//CGame::GetInstance().GetSoundFXPlayer()->SetSoundFXState(CSoundFX::NOSOUND);
			break;
		}
		case VK_UP:
		{
			if (CGame::GetInstance().GetModel()->GetControllerDirection() == DirectionUp) 
			{
				CGame::GetInstance().GetModel()->SetControllerDirection(DirectionNone);
			}
			
			//CGame::GetInstance().GetLevel()->GetBomberBoyBlack()->SetStopWalkingState(CBomberBoy::FACING_BACKWARD);
			//CGame::GetInstance().GetSoundFXPlayer()->SetSoundFXState(CSoundFX::NOSOUND);
			break;
		}
		case VK_DOWN:
		{
			if (CGame::GetInstance().GetModel()->GetControllerDirection() == DirectionDown) 
			{
				CGame::GetInstance().GetModel()->SetControllerDirection(DirectionNone);
			}
			
			//CGame::GetInstance().GetLevel()->GetBomberBoyBlack()->SetStopWalkingState(CBomberBoy::FACING_FORWARD);
			//CGame::GetInstance().GetSoundFXPlayer()->SetSoundFXState(CSoundFX::NOSOUND);
			break;
		}
		case VK_RIGHT:
		{
			if (CGame::GetInstance().GetModel()->GetControllerDirection() == DirectionRight) 
			{
				CGame::GetInstance().GetModel()->SetControllerDirection(DirectionNone);
			}
			
			CGame::GetInstance().GetModel()->SetControllerDirection(DirectionNone);
			//CGame::GetInstance().GetLevel()->GetBomberBoyBlack()->SetStopWalkingState(CBomberBoy::FACING_RIGHT);
			//CGame::GetInstance().GetSoundFXPlayer()->SetSoundFXState(CSoundFX::NOSOUND);
			break;
		}
		case VK_LEFT:
		{
			if (CGame::GetInstance().GetModel()->GetControllerDirection() == DirectionLeft) 
			{
				CGame::GetInstance().GetModel()->SetControllerDirection(DirectionNone);
			}
			
			//CGame::GetInstance().GetLevel()->GetBomberBoyBlack()->SetStopWalkingState(CBomberBoy::FACING_LEFT);
			//CGame::GetInstance().GetSoundFXPlayer()->SetSoundFXState(CSoundFX::NOSOUND);
			break;
		}
		case VK_RETURN:
		{
			CGame::GetInstance().RestartGame();
			
			break;	
		}
	}
}