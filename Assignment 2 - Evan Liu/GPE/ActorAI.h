#pragma once

#include "Model.h"
#include "ActorMover.h"

//==============================================================================
// CActorAI
//==============================================================================
class CActorAI : public IActorMoverHandler {

public:
    CActorAI(CModel *a_pModel, CActorData *a_pActorData);

    virtual ~CActorAI();

	virtual CActorData *GetActorData();

    virtual int GetTargetRow() override;

    virtual void SetTargetRow(int a_iRow) override;

    virtual int GetTargetColumn() override;

    virtual void SetTargetColumn(int a_iColumn) override;

    virtual bool CanMoveToTile(int a_iRow, int a_iColumn) override;

    virtual void ActivateMover(CActorMover *a_pMover);

    virtual void ActivateMover(CActorMover *a_pMover, float moveValue);

    virtual void Process(float a_fDeltaTick);

	virtual void Reset();

protected:
    CModel *m_pModel;

    CActorData *m_pActorData;
    CPosition *m_pActorOrigin;
    CPosition *m_pActorPosition;

    CActorLeftMover *m_pLeftMover;
    CActorRightMover *m_pRightMover;
    CActorUpMover *m_pUpMover;
    CActorDownMover *m_pDownMover;

    CActorMover *m_currentMover;

    int m_iTargetRow;
    int m_iTargetColumn;

    CActorMover *GetMover(EDirection a_eDirection);

    virtual void ProcessMove(float a_fMoveValue) = 0;

	virtual void InitTargetPosition();
};

//==============================================================================
// CPacmanAI
//==============================================================================
class CPacmanAI : public CActorAI {

public:

    CPacmanAI(CModel *a_pModel);

    virtual ~CPacmanAI();

    EDirection const &getControllerDirection() const;

    void setControllerDirection(EDirection const &a_eDirection);

    virtual void OnMoveComplete(CActorMover *mover, float remainder) override;

private:
    EDirection m_eControllerDirection;

protected:
    virtual void ProcessMove(float a_fMoveValue);
};

//==============================================================================
// CGhostAI
//==============================================================================
class CGhostAI : public CActorAI {

public:
    CGhostAI(CModel *a_pModel, CGhostData *a_pGhostData, float a_fIdleDelay);

    virtual ~CGhostAI();

    virtual void Process(float a_fDeltaTick) override;

    virtual void OnMoveComplete(CActorMover *mover, float remainder) override;

	virtual void Reset() override;

protected:
    virtual void ProcessMove(float a_fMoveValue) override;

private:
    CGhostData *m_pGhostData;

	float m_fIdleDelayTotal;

    float m_fIdleDelay;

    void ProcessIdle(float a_fDeltaTick);

    void StartHunt(float a_fDeltaTick);

    void FindMover(float a_fMoveValue);
};