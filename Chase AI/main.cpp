#include <SFML/Graphics.hpp>



int main(int argc, char** argv)
{
	const int PLAYER_MOVEMENT = 10;
	const int MOVEMENT = -1; // alter to change chase/evade tactics

	sf::RenderWindow window(sf::VideoMode(800, 600), "Chase AI");
	window.setFramerateLimit(30);

	sf::RectangleShape blue(sf::Vector2f(20,20));
	blue.setFillColor(sf::Color::Blue);
	blue.setPosition(0, 0);

	sf::RectangleShape red(sf::Vector2f(20, 20));
	red.setFillColor(sf::Color::Red);
	//red.setPosition(800-20, 600-20);
	red.setPosition(400, 300);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			bool moveMade = false;

			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			{
				sf::Vector2f position = blue.getPosition();
				position.x = position.x - PLAYER_MOVEMENT;
				blue.setPosition(position);

				moveMade = true;
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			{
				sf::Vector2f position = blue.getPosition();
				position.x = position.x + PLAYER_MOVEMENT;
				blue.setPosition(position);

				moveMade = true;
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			{
				sf::Vector2f position = blue.getPosition();
				position.y = position.y - PLAYER_MOVEMENT;
				blue.setPosition(position);

				moveMade = true;
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			{
				sf::Vector2f position = blue.getPosition();
				position.y= position.y + PLAYER_MOVEMENT;
				blue.setPosition(position);

				moveMade = true;
			}

			if (moveMade)
			{
				if (red.getPosition().x > blue.getPosition().x)
				{
					sf::Vector2f position = red.getPosition();
					position.x = position.x - MOVEMENT;
					red.setPosition(position);
				}
				else if (red.getPosition().x < blue.getPosition().x)
				{
					sf::Vector2f position = red.getPosition();
					position.x = position.x + MOVEMENT;
					red.setPosition(position);
				}

				if (red.getPosition().y > blue.getPosition().y)
				{
					sf::Vector2f position = red.getPosition();
					position.y = position.y - MOVEMENT;
					red.setPosition(position);
				}
				else if (red.getPosition().y < blue.getPosition().y)
				{
					sf::Vector2f position = red.getPosition();
					position.y = position.y + MOVEMENT;
					red.setPosition(position);
				}
			}
		}

		window.clear(sf::Color::Black);

		window.draw(blue);
		window.draw(red);

		window.display();
	}

	return 0;
}