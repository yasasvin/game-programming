#include "GL/glew.h"
#include "SFML/Graphics.hpp"
#include "SFML/OpenGL.hpp" // SFML OpenGL include
#include <iostream>
#include <cmath>

class Plane
{
	private:
		GLfloat vertices[12] = {10.f, -10.f, -10.f ,
								-10.0f, -10.f, -10.f,
								-10.f, -10.f, 10.f,
								10.f, -10.f, 10.f
		};

	public:
		Plane()
		{
			
		}
		void render()
		{
			glEnableClientState(GL_VERTEX_ARRAY);

			glVertexPointer(3, GL_FLOAT, 0, vertices);
			glColor3f(0, 1, 0);
			glDrawArrays(GL_QUADS, 0, 4);

			glDisableClientState(GL_VERTEX_ARRAY);
		}
};

class Particle
{
	private:
		GLUquadric *quad = gluNewQuadric();
	public:
		sf::Vector3f location;
		sf::Vector3f velocity;
		sf::Vector3f acceleration;

		void integrate()
		{
			location = location + velocity;
			velocity = velocity + acceleration;
		}

		bool distance(sf::Vector3f normal, sf::Vector3f plane)
		{
			sf::Vector3f temp = location - plane;

			float scale = product(temp, normal);
			
			sf::Vector3f projection = normal * scale;
			
			float dist = magnitude(projection);

			std::cout << "distance:" << dist << std::endl;

			if (dist <= 2.0f)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		float product(sf::Vector3f a, sf::Vector3f b)
		{
			return a.x * b.x + a.y * b.y + a.z * b.z;
		}
		
		sf::Vector3f normalize(sf::Vector3f vec)
		{
			float m = magnitude(vec);

			return vec / m;
		};


		float magnitude(sf::Vector3f vector)
		{
			return std::sqrtf(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
		}

		void render()
		{
			glTranslatef(location.x, location.y, location.z);
			glColor3f(1, 0, 0);
			gluSphere(quad, 1.0f, 8, 8);
		}
};

int main()
{
	bool texturesOn = false;
	bool wireframe = false;

	// Create the main window
	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML OpenGL");


	//window.setFramerateLimit(100);

	// Create a clock for measuring time elapsed
	sf::Clock Clock;

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // nicest perspective correction calculations

	//prepare OpenGL surface for HSR
	glClearDepth(1.f); // clear the z-buffer/depth buffer completely +1.0f is the furtherest away
	glClearColor(0.3f, 0.3f, 0.3f, 0.f); // set the background colour for when we clear the screen RGBA values in the 0.0 to 1.0 range. This gives us a nice grey background.

	Particle particle;

	if (texturesOn)
	{
		sf::Image texture;
		texture.loadFromFile("earth.bmp");

		GLuint texture_handle;
		glGenTextures(1, &texture_handle); // allocate a texture handle within OpenGL
		glBindTexture(GL_TEXTURE_2D, texture_handle); // bind to the texture handle target

		// import texture into OpenGL
		glTexImage2D(
			GL_TEXTURE_2D, 0, GL_RGBA,
			texture.getSize().x, texture.getSize().y,
			0,
			GL_RGBA, GL_UNSIGNED_BYTE, texture.getPixelsPtr()
		);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		glEnable(GL_TEXTURE_2D); // enable 2D textures
	}

	if (!wireframe)
	{
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);

		glShadeModel(GL_FLAT);
	}
	// Setup a perspective projection & Camera position

	// GL_PROJECTION what we actually see
	glMatrixMode(GL_PROJECTION); // Select the builtin projection matrix
	glLoadIdentity();  // reset the projection matrix by loading the projection identity matrix

	GLdouble fovY = 90;
	GLdouble aspect = 1.0f;
	GLdouble zNear = 1.0f; 
	GLdouble zFar = 300.0f;

	const GLdouble pi = 3.1415926535897932384626433832795;
	GLdouble fW, fH;

	fH = tan(fovY / 360 * pi) * zNear;
	fW = fH * aspect;

	float angle = 0;

	// define a perspective projection
	glFrustum(-fW, fW, -fH, fH, zNear, zFar); // multiply the set matrix; by a perspective matrix

	GLfloat DIFFUSE_COLOR[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat SPECULAR_COLOR[] = { 1.25f, 1.25f, 1.25f, 1.0f };
	GLfloat LIGHT_POSITION[] = { 0.0f, 3, 0, -8.0f };
	glLightfv(GL_LIGHT0, GL_POSITION, LIGHT_POSITION);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, DIFFUSE_COLOR);
	glLightfv(GL_LIGHT0, GL_SPECULAR, SPECULAR_COLOR);

	particle.location.x = -10;
	particle.location.y = 10;
	particle.location.z = 0;

	particle.acceleration.y = -0.0001f;
	particle.acceleration.x = 0.000001f;

	// Start game loop
	while (window.isOpen())
	{
		// Process events
		sf::Event Event;
		while (window.pollEvent(Event))
		{
			// Close window : exit
			if (Event.type == sf::Event::Closed)
				window.close();
		}

		//Prepare for drawing
		// Clear color and depth buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear z-buffer and set previously selected colour

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity(); // reset

		glEnable(GL_CULL_FACE); // enable back face culling so we don't see the hidden vertices / textures of the cube
		glCullFace(GL_BACK);

		
		//glRotatef(45, 1, 0, 0);
		glTranslatef(0, 0,-15);
		
		//glRotatef(angle+=0.01f, 1, 1, 0);
		if (texturesOn)
		{
			//gluQuadricTexture(quad, 1);
		}
		else
		{
			glColor3f(1, 0, 0);
			if (wireframe)
			{
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			}
			else
			{
				glEnable(GL_COLOR_MATERIAL);
			}
		}		

		Plane plane;
		plane.render();

		particle.integrate();
		particle.render();

		sf::Vector3f corner(10.0f, -10.0f, -10.0f);
		sf::Vector3f normal(0, 1, 0);
		if (particle.distance(normal, corner))
		{
			sf::Vector3f reflected = 2.0f * normal * (particle.product(normal, particle.velocity));
			particle.velocity = particle.velocity - reflected;
		}

		window.display();
	}

	return EXIT_SUCCESS;
}