#include "Spring.h"

#include <iostream>

sf::Vector2f Spring::getOrigin()
{
	return anchor.getLocation();
}

void Spring::setOrigin(const sf::Vector2f &origin)
{
	this->anchor.setLocation(origin);
}

float Spring::getConstant()
{
	return k;
}

float Spring::getRestLength()
{
	return restLength;
}



void Spring::update(const sf::Time & deltaTime)
{
	/*
	sf::Vector2f force = weight.getLocation() - anchor.getLocation();
	weight.applyForce(-force);

	weight.update(deltaTime);
	*/

	
	sf::Vector2f force = weight.getLocation() - anchor.getLocation();

	float displacement = vm::magnitude(force) - restLength;

	force = vm::normalize(force);
	force =  force * -1.0f * k *  displacement;

	sf::Vector2f acceleration = force / weight.getMass();

	weight.applyForce(sf::Vector2f(0, 30.0f));
	weight.applyForce(acceleration);
	
	weight.update(deltaTime);

	// comment out updates to the anchor we don't want to move that (yet)

	//anchor.applyForce(-acceleration);
	//anchor.update(deltaTime);
}

Particle& Spring::getParticle()
{
	return weight;
}

void Spring::setParticle(Particle & particle)
{
	this->weight = particle;
}

Spring::Spring(Particle& origin, Particle& weight, float springLength) : anchor(origin), weight(weight)
{
	this->restLength = springLength;
	this->k = 5.0f;
}


Spring::~Spring()
{
}
