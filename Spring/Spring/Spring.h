#include "Particle.h"
#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>
#include <SFML\Graphics\Rect.hpp>
#include "VectorMath.h"

#pragma once
class Spring
{
	Particle &anchor;
	Particle &weight;

	float restLength;
	float k;
public:
	sf::Vector2f getOrigin();
	void setOrigin(const sf::Vector2f &origin);

	float getConstant();
	float getRestLength();

	void update(const sf::Time &deltaTime);
	Particle& getParticle();
	void setParticle(Particle &particle);

	Spring(Particle& origin, Particle& weight, float springLength);
	~Spring();
};

