#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <random>
#include <list>
#include <cmath>


#include "Particle.h"
#include "Spring.h"
#include "VectorMath.h"

bool mouseDown = false;

void processEvents(Spring &spring, const sf::FloatRect &rect, sf::RenderWindow &window)
{
	Particle particle = spring.getParticle();

	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
		else if (event.type == sf::Event::MouseButtonPressed)
		{
			mouseDown = true;

			sf::Vector2f location = sf::Vector2f(event.mouseButton.x, event.mouseButton.y);

			particle.setLocation(location);
			particle.setVelocity(sf::Vector2f());
			particle.setAcceleration(sf::Vector2f());

			spring.setParticle(particle);
		}
		else if (event.type == sf::Event::MouseMoved)
		{
			if (mouseDown)
			{
				sf::Vector2f location = sf::Vector2f(event.mouseMove.x, event.mouseMove.y);

				particle.setLocation(location);
				particle.setVelocity(sf::Vector2f());
				particle.setAcceleration(sf::Vector2f());

				spring.setParticle(particle);
			}
		}
		else if (event.type == sf::Event::MouseButtonReleased)
		{
			mouseDown = false;
		}
	}
}

int main()
{
	sf::Time TimePerFrame = sf::seconds(1.f / 60);

	sf::ContextSettings context;
	context.antialiasingLevel = 10;

	sf::RenderWindow window(sf::VideoMode(1024, 768), "SFML Springs", sf::Style::Default, context);

	const sf::FloatRect rect = sf::FloatRect(sf::Vector2f(0, 0), sf::Vector2f(window.getSize()));

	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;

	Particle anchor = Particle(rect, sf::Vector2f(window.getSize().x / 2, (window.getSize().y / 2)), 1);
	Particle weight = Particle(rect, sf::Vector2f(window.getSize().x / 2, (window.getSize().y / 2) + 200), 1);

	Spring spring = Spring(anchor, weight, 200);

	while (window.isOpen())
	{
		processEvents(spring, rect, window);

		timeSinceLastUpdate += clock.restart();

		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;
			processEvents(spring, rect, window);

			spring.update(TimePerFrame);
		}

		window.clear();
		sf::CircleShape origin = sf::CircleShape(5);
		origin.setOrigin(5, 5);
		origin.setPosition(spring.getOrigin());
		origin.setFillColor(sf::Color::Red);

		window.draw(origin);

		sf::CircleShape weight = sf::CircleShape(20);
		weight.setOrigin(20, 20);
		weight.setPosition(spring.getParticle().getLocation());
		weight.setFillColor(sf::Color::Red);

		sf::Vertex vertices[2] =
		{
			sf::Vertex(spring.getOrigin()),
			sf::Vertex(spring.getParticle().getLocation())
		};

		window.draw(vertices, 2, sf::Lines);

		window.draw(weight);

		window.display();
	}
}

