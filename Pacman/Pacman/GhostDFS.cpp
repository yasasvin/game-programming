#include "GhostDFS.h"

const int BLOCK_SZ = 16;

GhostDFS::GhostDFS(Pacman * pacman, const char * filename) : Ghost(pacman,filename)
{
}

void GhostDFS::walk(Map &map, sf::RenderWindow & window, bool debug)
{
	int source = map.convert2Dto1D(getRow(), getColumn());

	std::list <int> edges = map.getGraph()->at(source);
	std::list <int> path;

	visited.clear();

	int target = dfs(map, window, map.getGraph(), edges, map.convert2Dto1D(pacman->getRow(), pacman->getColumn()), path, debug);

	std::cout << path.front() << std::endl;

	int destination = path.front();
	std::pair<int, int> pair = map.convert1Dto2D(destination);

	std::cout << std::endl << getRow() << "," << getColumn() << std::endl;
	std::cout << pair.first << "," << pair.second << std::endl;

	int distY = pair.first - getRow();
	int distX = pair.second - getColumn();

	sf::Vector2f pos = sprite.getPosition();
	
	if (map.isTileIntersection(getRow(), getColumn()) && rowBoundary() && columnBoundary())
	{
		if (distX < 0)
		{
			facing = LEFT;
			moveLEFT(pos);
		}
		else if (distX >0)
		{
			facing = RIGHT;
			moveRIGHT(pos);
		}
		else if (distY < 0)
		{
			facing = UP;
			moveUP(pos);
		}
		else if (distY >0)
		{
			facing = DOWN;
			moveDOWN(pos);
		}
	}
	else if (facing == UP)
	{
		moveUP(pos);
	}
	else if (facing == DOWN)
	{
		moveDOWN(pos);
	}
	else if (facing == LEFT)
	{
		moveLEFT(pos);
	}
	else if (facing == RIGHT)
	{
		moveRIGHT(pos);
	}

	sprite.setPosition(pos);
	frame = (frame + 1) % 3;
}

int GhostDFS::dfs(Map &map, sf::RenderWindow & window, std::unordered_map <int, std::list<int>> *graph, std::list <int> edges, int target, std::list <int> &path, bool debug)
{
	for (std::list<int>::iterator it = edges.begin(); it != edges.end(); it++)
	{
		if (visited.count(*it) == 0)
		{
			path.push_back(*it);

			visited.insert(*it);
			
			if (debug)
			{
				std::pair<int, int> pair = map.convert1Dto2D(*it);
				sf::RectangleShape rectangle;
				rectangle.setPosition(pair.second * BLOCK_SZ, pair.first * BLOCK_SZ);
				rectangle.setSize(sf::Vector2f(BLOCK_SZ, BLOCK_SZ));

				sf::Color color = sf::Color::Green;

				rectangle.setFillColor(color);

				window.draw(rectangle);
			}

			if (*it == target)
			{
				return target;
			}
			else
			{
				std::list <int> newEdges = graph->at(*it);
				int targetCell = dfs(map, window, map.getGraph(), newEdges, target, path, true);

				if (targetCell != -1)
				{
					return targetCell;
				}
			}
		}
	}
	return -1;
}

GhostDFS::~GhostDFS()
{
}
