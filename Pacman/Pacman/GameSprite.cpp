#include "GameSprite.h"

bool GameSprite::rowBoundary()
{
	return ((int)sprite.getPosition().y % 16 == 0);
}
bool GameSprite::columnBoundary()
{
	return ((int)sprite.getPosition().x % 16 == 0);
}
int GameSprite::getColumn()
{
	return sprite.getPosition().x / 16;
}

int GameSprite::getRow()
{
	return sprite.getPosition().y / 16;
}

void GameSprite::walk(Map & map)
{
}


sf::Sprite GameSprite::getSprite()
{
	return sprite;
}

void GameSprite::setFacing(Facing facing)
{
	this->facing = facing;
}

void GameSprite::setPosition(int row, int column)
{
	sprite.setPosition(row * 16, column * 16);
}


GameSprite::GameSprite()
{
}


GameSprite::~GameSprite()
{
}
