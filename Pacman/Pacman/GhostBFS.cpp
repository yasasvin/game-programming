#include "GhostBFS.h"

#include "VectorMath.h"

const int BLOCK_SZ = 16;


GhostBFS::GhostBFS(Pacman * pacman, const char* filename) : Ghost(pacman,filename)
{

}

int GhostBFS::bfs(Map &map, std::unordered_map <int, std::list<int>> graph, sf::RenderWindow &window, int source, int target, bool debug)
{
	std::unordered_set<int> visited;

	std::queue<int> queue;

	int root = source;

	queue.push(root); // enqueue the root node
	visited.insert(root);

	std::list<int> path;

	const int parentSz = Map::COLUMN_COUNT * Map::ROW_COUNT;

	int parents[parentSz];

	parents[root] = -1;

	while (!queue.empty())
	{
		int node = queue.front();
		queue.pop();

		path.push_back(node);

		if (debug)
		{
			std::pair<int, int> pair = map.convert1Dto2D(node);
			sf::RectangleShape rectangle;
			rectangle.setPosition(pair.second * BLOCK_SZ, pair.first * BLOCK_SZ);
			rectangle.setSize(sf::Vector2f(BLOCK_SZ, BLOCK_SZ));

			sf::Color color = sf::Color::Green;

			rectangle.setFillColor(color);

			window.draw(rectangle);
		}

		if (node == target)
		{
			int path = 0;

			int parent = node;

			while (parent != -1)
			{
				parent = parents[parent];
				path++;
			}
			parent = node;

			float progress = (float)path;
			int prev = -1;
			while (parent != root)
			{
				prev = parent;
				parent = parents[parent];

				if (debug)
				{
					std::pair<int, int> pair = map.convert1Dto2D(parent);

					sf::RectangleShape rectangle;
					rectangle.setPosition(pair.second * BLOCK_SZ, pair.first * BLOCK_SZ);
					rectangle.setSize(sf::Vector2f(BLOCK_SZ, BLOCK_SZ));

					sf::Color color = sf::Color::Magenta;
					color.a = (int)((progress / path) * 255.0f);

					rectangle.setFillColor(color);

					window.draw(rectangle);
				}
				progress = progress - 1.0f;
			}

			if (prev == -1)
			{
				return root;
			}
			else
			{
				return prev;
			}
		}

		std::list <int> edges = graph[node];

		for (std::list<int>::iterator it = edges.begin(); it != edges.end(); it++)
		{
			int nde = *it;

			if (visited.count(nde) == 0)
			{
				visited.insert(nde);
				queue.push(nde);

				parents[nde] = node;
			}
		}
	}
}

void GhostBFS::walk(Map &map,sf::RenderWindow &window, bool debug)
{
	std::unordered_map <int, std::list<int>>* graph = map.getGraph();

	int ghost = map.convert2Dto1D(getRow(), getColumn());
	int player = map.convert2Dto1D(pacman->getRow(), pacman->getColumn());

	int destination = bfs(map,*graph, window, ghost, player, debug);

	std::pair<int, int> pair = map.convert1Dto2D(destination);

	std::cout << std::endl << getRow() << "," << getColumn() << std::endl;
	std::cout << pair.first << "," << pair.second << std::endl;

	int distY = pair.first - getRow();
	int distX = pair.second - getColumn();

	sf::Vector2f pos = sprite.getPosition();

	if (map.isTileIntersection(getRow(), getColumn()) && rowBoundary() && columnBoundary())
	{
		if (distX < 0)
		{
			moveLEFT(pos);
		}
		else if (distX >0)
		{
			moveRIGHT(pos);
		}
		else if (distY < 0)
		{
			moveUP(pos);
		}
		else if (distY >0)
		{
			moveDOWN(pos);
		}
	}
	else if (facing == UP)
	{
		moveUP(pos);
	}
	else if (facing == DOWN)
	{
		moveDOWN(pos);
	}
	else if (facing == LEFT)
	{
		moveLEFT(pos);
	}
	else if (facing == RIGHT)
	{
		moveRIGHT(pos);
	}

	sprite.setPosition(pos);
	frame = (frame + 1) % 3;
}

GhostBFS::~GhostBFS()
{
}
