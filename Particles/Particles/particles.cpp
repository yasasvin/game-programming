#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include "Particle.h"
#include "VectorMath.h"

void processEvents(sf::RenderWindow &window, Particle &particle)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
		else if (event.type == sf::Event::MouseMoved)
		{
			sf::Vector2f mousePosition = sf::Vector2f(event.mouseMove.x, event.mouseMove.y);

			sf::Vector2f difference = mousePosition - particle.getLocation();

			sf::Vector2f norm = vm::normalize(difference);

			std::cout << vm::magnitude(norm) << std::endl;

			norm = norm * 2.0f;

			std::cout << norm.x << "," << norm.y << std::endl;

			//particle.setVelocity(sf::Vector2f());
			particle.setAcceleration(difference);
		}
	}

}



int main()
{
	sf::Time TimePerFrame = sf::seconds(1.f / 60.f);

	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML works!");
	window.setFramerateLimit(60);

	sf::CircleShape circle(20);
	circle.setFillColor(sf::Color(sf::Color::Red));

	Particle particle(sf::FloatRect(sf::Vector2f(0, 0), sf::Vector2f(window.getSize())), sf::Vector2f(window.getSize().x / 2, 0), 1);
	
	//particle.applyForce(sf::Vector2f(0, 9.8));

	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;
	while (window.isOpen())
	{
		processEvents(window, particle);
		timeSinceLastUpdate += clock.restart();

		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;
			processEvents(window, particle);
			
			particle.update(TimePerFrame);
			particle.checkEdges();
		}

		circle.setPosition(particle.getLocation());

		window.clear();
		window.draw(circle);
		window.display();
	}
}