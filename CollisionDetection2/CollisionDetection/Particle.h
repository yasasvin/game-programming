#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>
#include <SFML\Graphics\Rect.hpp>
#include <list>
#pragma once
class Particle
{
	private:
		sf::CircleShape circle;
		sf::Vector2f location; // difference between location and the origin
		sf::Vector2f velocity; // the rate of change in location
		sf::Vector2f acceleration; // the rate of change in velocity
		float mass;
		sf::FloatRect boundary;
		int life = 255;
		bool collision;
	public:
		sf::CircleShape& getCircleShape();
		void setCircleShape(sf::CircleShape& circle);
		sf::Vector2f getVelocity();
		void setVelocity(const sf::Vector2f &velocity);
		void setAcceleration(const sf::Vector2f &acceleration);
		sf::Vector2f getAcceleration();
		void resetAcceleration();

		int getLife();
		void applyForce(const sf::Vector2f &force);

		bool getCollision();
		void setCollision(bool collision);

		sf::Vector2f getLocation();
		void setLocation(const sf::Vector2f &location);
		void update(const sf::Time &deltaTime);
		void checkEdges();
		bool checkCollision(std::list<Particle> &particles);
		Particle(const sf::FloatRect &boundary);
		Particle(const sf::FloatRect &boundary, const  sf::Vector2f &location, const float &mass);
		Particle();
		~Particle();
};

