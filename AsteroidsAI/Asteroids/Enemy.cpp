#include "Enemy.h"
#include "VectorMath.h"
#include <iostream>


Enemy::Enemy()
{
}

Enemy::Enemy(const sf::FloatRect & boundary, const sf::Vector2f & location, const float & mass) : SpaceShip(boundary, location, mass)
{
	this->mainRocket.setFillColor(sf::Color::Cyan);
	this->leftThruster.setFillColor(sf::Color::Cyan);
	this->rightThruster.setFillColor(sf::Color::Cyan);

	this->maxThrust = 5.5;
}

Enemy::~Enemy()
{
}

void Enemy::track(SpaceShip & player)
{
	sf::Vector2f relative = player.getLocation() - getLocation();

	angle = vm::getAngle(relative);

	thrust = vm::setAngle(thrust, angle);

	sf::Vector2f closing = player.getVelocity() - getVelocity();
	setVelocity(closing);
	velocity = vm::setAngle(velocity, angle);
}

