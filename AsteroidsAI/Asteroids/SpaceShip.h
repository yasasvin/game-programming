#pragma once

#include "Particle.h"
#include <random>

class SpaceShip : public Particle
{
	protected:

		sf::CircleShape mainRocket;
		sf::RectangleShape leftThruster;
		sf::RectangleShape rightThruster;
		sf::Vector2f thrust;
		float angle;
		float maxThrust = 5.0;
	public:
		SpaceShip(const sf::FloatRect &boundary, const  sf::Vector2f &location, const float &mass);
		SpaceShip();
		~SpaceShip();

		void update(const sf::Time &deltaTime);

		void rotateLeft();
		void rotateRight();
		void fireRocket();
		void stop();
		void draw(sf::RenderWindow *window);
};

