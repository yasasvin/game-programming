#include "Particle.h"
#include "SpaceShip.h"

Particle::Particle()
{
	location = sf::Vector2f(0, 0);
	velocity = sf::Vector2f(0, 0);
	acceleration = sf::Vector2f(0, 0);
	boundary = sf::FloatRect();
}

Particle::Particle(const sf::FloatRect &boundary, const  sf::Vector2f &location,const  float &mass)
{
	this->location = location;
	this->velocity = sf::Vector2f();
	this->acceleration = sf::Vector2f();
	this->boundary = boundary;
	this->mass = mass;

	this->topSpeed = 200;
}

void Particle::setVelocity(const sf::Vector2f &velocity)
{
	this->velocity = velocity;
}

void Particle::setAcceleration(const sf::Vector2f &acceleration)
{
	this->acceleration = acceleration;
}

sf::Vector2f Particle::getAcceleration()
{
	return acceleration;
}


sf::Vector2f Particle::getVelocity()
{
	return velocity;
}

sf::Vector2f Particle::getLocation()
{
	return location;
}

void Particle::applyForce(const sf::Vector2f &force)
{
	acceleration = acceleration + (force / mass);
}

void Particle::update(const sf::Time &deltaTime)
{
	velocity = velocity + (acceleration * deltaTime.asSeconds());
	location = location + (velocity * deltaTime.asSeconds());

	if (life > 0)
	{
		life--;
	}

	// don't go any faster than the topSpeed parameter
	if (vm::magnitude(velocity) > topSpeed)
	{
		velocity = vm::setMagnitude(velocity, topSpeed);
	}
	// slow things down due friction
	velocity = velocity * 0.995f;
}

int Particle::getLife()
{
	return life;
}

void Particle::checkEdges()
{
	if ((location.x > boundary.width))
	{
		location.x = 0;
	}

	if ((location.x < 0))
	{
		location.x = boundary.width;
	}

	if ((location.y > boundary.height))
	{
		location.y = 0;
	}

	if (location.y < 0)
	{
		location.y = boundary.height;
	}
}

Particle::~Particle()
{
}
