#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>
#include <SFML\Graphics\Rect.hpp>
#include <list>
#pragma once
const int PARTICLE_RADIUS = 15;
class Particle
{
	private:
		sf::Vector2f top;
		sf::Vector2f left;

		sf::CircleShape circle;
		sf::Vector2f location; // difference between location and the origin
		sf::Vector2f velocity; // the rate of change in location
		sf::Vector2f acceleration; // the rate of change in velocity
		float mass;
		sf::FloatRect boundary;
		int life = 255;
		bool collision;
	public:
		sf::Vector2f getTop();

		sf::CircleShape& getCircleShape();
		void setCircleShape(sf::CircleShape& circle);
		sf::Vector2f getVelocity();
		void setVelocity(const sf::Vector2f &velocity);
		void setAcceleration(const sf::Vector2f &acceleration);
		sf::Vector2f getAcceleration();
		void resetAcceleration();

		int getLife();
		void applyForce(const sf::Vector2f &force);

		bool getCollision();
		void setCollision(bool collision);

		sf::Vector2f getLocation();
		void setLocation(const sf::Vector2f &location);
		void update(const sf::Time &deltaTime);
		bool checkCollision(std::list<Particle> &particles);
		void checkEdges();
		void checkWalls(sf::Vector2f start, sf::Vector2f end);
		void drawVector(sf::RenderWindow &window, sf::Vector2f start);
		void drawPerp(sf::RenderWindow &window,sf::Vector2f start, sf::Vector2f perp);
		void drawProjection(sf::RenderWindow &window, sf::Vector2f start, sf::Vector2f end);
		void draw(sf::RenderWindow &window);
		
		Particle(const sf::FloatRect &boundary);
		Particle(const sf::FloatRect &boundary, const  sf::Vector2f &location, const float &mass);
		Particle();
		~Particle();
};

