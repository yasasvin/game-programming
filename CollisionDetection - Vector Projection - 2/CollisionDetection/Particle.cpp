#include "Particle.h"
#include "VectorMath.h"
#include <iostream>

Particle::Particle()
{
	location = sf::Vector2f(0, 0);
	velocity = sf::Vector2f(0, 0);
	acceleration = sf::Vector2f(0, 0);

	boundary = sf::FloatRect();
	this->circle = sf::CircleShape(2);
	this->collision = false;

	this->top = sf::Vector2f(boundary.width, 0);
	this->left = sf::Vector2f(0, boundary.height);
}

Particle::Particle(const sf::FloatRect &boundary)
{
	location = sf::Vector2f(0, 0);
	velocity = sf::Vector2f(1, 1);
	acceleration = sf::Vector2f(1, 1);

	this->boundary = boundary;
	this->circle = sf::CircleShape(2);
	this->collision = false;

	this->top = sf::Vector2f(boundary.width, 0);
	this->left = sf::Vector2f(0, boundary.height);
}

Particle::Particle(const sf::FloatRect &boundary, const  sf::Vector2f &location,const  float &mass)
{
	this->location = location;
	this->velocity = sf::Vector2f();
	this->acceleration = sf::Vector2f();
	this->boundary = boundary;
	this->mass = mass;

	this->circle = sf::CircleShape(2);
	this->collision = false;

	this->top = sf::Vector2f(boundary.width, 0);
	this->left = sf::Vector2f(0, boundary.height);
}

void Particle::setVelocity(const sf::Vector2f &velocity)
{
	this->velocity = velocity;
}

void Particle::setAcceleration(const sf::Vector2f &acceleration)
{
	this->acceleration = acceleration;
}

sf::Vector2f Particle::getAcceleration()
{
	return acceleration;
}

sf::Vector2f Particle::getVelocity()
{
	return velocity;
}

sf::Vector2f Particle::getLocation()
{
	return location;
}

void Particle::setLocation(const sf::Vector2f & location)
{
	this->location = location;
}

void Particle::applyForce(const sf::Vector2f &force)
{
	acceleration = acceleration + (force / mass);
}

bool Particle::getCollision()
{
	return collision;
}

void Particle::setCollision(bool collision)
{
	this->collision = collision;
}

void Particle::update(const sf::Time &deltaTime)
{
	velocity = velocity + (acceleration * deltaTime.asSeconds());
	location = location + (velocity * deltaTime.asSeconds());

	circle.setPosition(location);
}

sf::Vector2f Particle::getTop()
{
	return top;
}

sf::CircleShape& Particle::getCircleShape()
{
	return circle;
}
void Particle::setCircleShape(sf::CircleShape& circle)
{
	this->circle = circle;
}

int Particle::getLife()
{
	return life;
}
void Particle::resetAcceleration()
{
	acceleration.x = 0.0f;
	acceleration.y = 0.0f;
}

void Particle::checkEdges()
{
	if (location.y <0)
	{
		sf::Vector2f normalTop = vm::normalize(vm::perpcw(top));
		velocity = velocity - (normalTop * 2.0f) * vm::product(velocity, normalTop);
	}
	if (location.x < 0)
	{
		sf::Vector2f normalLeft = vm::normalize(vm::perpcw(left));
		velocity = velocity - (normalLeft * 2.0f) * vm::product(velocity, normalLeft);
	}

	if(location.x > boundary.width) {
		velocity.x = velocity.x * -1;
	}
	if(location.y > boundary.height) {
		velocity.y = velocity.y * -1;
	}
}

void Particle::checkWalls(sf::Vector2f start, sf::Vector2f end)
{
	sf::Vector2f line = end - start;

	sf::Vector2f perp = vm::perpccw(line);

	sf::Vector2f unit = vm::normalize(perp); // get the unit vector of our perpendicular vector

	sf::Vector2f distance = getLocation() - start;

	float scale = vm::product(distance, unit); // calculate the scale factor using the dot-product
	sf::Vector2f projected = scale * unit; // scale the unit vector by the scale factor that we have calculated

	// possible collision
	if (vm::magnitude(projected) < PARTICLE_RADIUS) 
	{
		// check if are within the range of the line segment
	
		// project the distance vector onto the line vector to give us the distance from start to the current location vector
		sf::Vector2f lineUnit = vm::normalize(line); // calculate the unit vector of the line vector

		float projection = vm::product(distance, lineUnit);
		sf::Vector2f lineProjection = projection * lineUnit;

		// is the dot product of the start and distance vectors positive?
		if (vm::product(distance, start)>0 && vm::magnitude(lineProjection) <= vm::magnitude(line))
		{
			velocity = velocity - (unit * 2.0f) * vm::product(velocity, unit);
		}
	}
}

void Particle::drawVector(sf::RenderWindow & window, sf::Vector2f start)
{
	sf::Vertex vector[2] =
	{
		sf::Vertex(start, sf::Color::Red),
		sf::Vertex(getLocation(), sf::Color::Red)
	};
	window.draw(vector, 2, sf::Lines);
}

void Particle::drawPerp(sf::RenderWindow & window, sf::Vector2f start, sf::Vector2f perp)
{
	sf::Vector2f distance = getLocation() - start;

	sf::Vector2f unit = vm::normalize(perp); // get the unit vector of our perpendicular vector

	float scale = vm::product(distance, unit); // calculate the scale factor using the dot-product
	sf::Vector2f projected = scale * unit; // scale the unit vector by the scale factor that we have calculated

	sf::Vertex length[2] =
	{
		sf::Vertex(start, sf::Color::Blue),
		sf::Vertex(start + projected, sf::Color::Blue)
	};
	window.draw(length, 2, sf::Lines);
}

void Particle::drawProjection(sf::RenderWindow & window, sf::Vector2f start, sf::Vector2f end)
{
	sf::Vector2f line = end - start;

	sf::Vector2f lineUnit = vm::normalize(line); // calculate the unit vector of the line vector

	sf::Vector2f distance = getLocation() - start;
	float projection = vm::product(distance, lineUnit);
	sf::Vector2f lineProjection = projection * lineUnit;

	sf::Vertex length[2] =
	{
		sf::Vertex(start, sf::Color::Blue),
		sf::Vertex(start + lineProjection, sf::Color::Blue)
	};
	window.draw(length, 2, sf::Lines);
}

void Particle::draw(sf::RenderWindow & window)
{
	window.draw(circle);
}

bool Particle::checkCollision(std::list<Particle> &particles)
{
	for (std::list<Particle>::iterator it = particles.begin(); it != particles.end(); ++it)
	{	
		sf::Vector2f difference = location - it->getLocation();

		float distance = vm::magnitude(difference);

		float radiusSum = circle.getRadius() + it->getCircleShape().getRadius();

		if (collision)
		{
			if (distance != 0.0f)
			{
				if (distance > radiusSum)
				{
					collision = false;
				}
			}
		}
		else
		{
			if (distance != 0.0f)
			{
				if (distance <= radiusSum)
				{
					velocity = velocity * -1.0f;

					collision = true;
					break;
				}
			}
		}
	}

	if (collision)
	{
		circle.setFillColor(sf::Color::Red);
	}
	else
	{
		circle.setFillColor(sf::Color::Green);
	}
	return collision;
}

Particle::~Particle()
{
}
