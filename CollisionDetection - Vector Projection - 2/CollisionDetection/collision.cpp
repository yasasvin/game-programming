#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <random>
#include <list>
#include <cmath>

#include "Particle.h"
#include "VectorMath.h"

const int PARTICLE_SZ = 5;

const int SPEED = 200;

const int WIDTH = 1024;
const int HEIGHT = 768;

std::default_random_engine generator;
std::uniform_real_distribution<float> vx(-SPEED, +SPEED);
std::uniform_real_distribution<float> vy(-SPEED, +SPEED);

std::uniform_real_distribution<float> x(0,WIDTH);
std::uniform_real_distribution<float> y(0,HEIGHT);

void createParticle(std::list <Particle> &particles, const sf::FloatRect &rect, sf::RenderWindow &window, const sf::Vector2f &location)
{
	sf::Color colour = sf::Color::Green;

	Particle particle = Particle(rect, sf::Vector2f(x(generator),y(generator)), 1);
	particle.setVelocity(sf::Vector2f(vx(generator), vy(generator)));

	sf::CircleShape circle = particle.getCircleShape();

	circle.setOrigin(sf::Vector2f(PARTICLE_RADIUS/2,PARTICLE_RADIUS/2));
	circle.setRadius(PARTICLE_RADIUS);
	circle.setFillColor(colour);

	particle.setCircleShape(circle);

	while (particle.checkCollision(particles))
	{
		particle.setLocation(sf::Vector2f(x(generator), y(generator)));
		particle.setCollision(false);
	}
	
	particles.push_front(particle);
}


void processEvents(std::list <Particle> &particles, const sf::FloatRect &rect, sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
	}
}

int main()
{
	float angle = 0;
	sf::Time TimePerFrame = sf::seconds(1.f / 120.f);

	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;

	sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "SFML Collision Detection and Response", sf::Style::Default, settings);
	window.setFramerateLimit(120);

	std::list <Particle> particles;

	const sf::FloatRect rect = sf::FloatRect(sf::Vector2f(0, 0), sf::Vector2f(window.getSize()));

	for (int i = 0; i < PARTICLE_SZ; i++)
	{
		createParticle(particles, rect, window, sf::Vector2f(window.getSize().x / 2, window.getSize().y / 2));
	}

	sf::Vector2f start(250,200);
	sf::Vector2f end(600, 500);

	sf::Vector2f line = end - start;
	sf::Vector2f perp = vm::perpccw(line);

	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;
	while (window.isOpen())
	{
		window.clear();

		processEvents(particles, rect, window);

		timeSinceLastUpdate += clock.restart();

		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;
			processEvents(particles, rect, window);
		
			sf::Vertex vertices[2] =
			{
				sf::Vertex(start, sf::Color::White),
				sf::Vertex(end, sf::Color::White)
			};
			window.draw(vertices, 2, sf::Lines);

			sf::Vertex normal[2] =
			{
				sf::Vertex(start, sf::Color::Green),
				sf::Vertex(start + perp, sf::Color::Green)
			};
			window.draw(normal, 2, sf::Lines);

			for(std::list<Particle>::iterator it = particles.begin(); it != particles.end(); ++it)
			{
				it->checkEdges();
				it->checkCollision(particles);
				it->checkWalls(start, end);
				it->update(TimePerFrame);
			}
		}
		for (std::list<Particle>::iterator it = particles.begin(); it != particles.end(); ++it)
		{
			it->drawVector(window, start);
			it->draw(window);
			it->drawPerp(window, start, perp);
			it->drawProjection(window, start, end);
		}
		window.display();
	}
}

