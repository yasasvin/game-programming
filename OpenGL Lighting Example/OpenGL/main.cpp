#include "SFML/Graphics.hpp"
#include "SFML/OpenGL.hpp" // SFML OpenGL include
#include <iostream>

int main()
{
	// Create the main window
	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML OpenGL");

	// Create a clock for measuring time elapsed
	sf::Clock Clock;

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // nicest perspective correction calculations

	//prepare OpenGL surface for HSR
	glClearDepth(1.f); // clear the z-buffer/depth buffer completely +1.0f is the furtherest away
	glClearColor(0.3f, 0.3f, 0.3f, 0.f); // set the background colour for when we clear the screen RGBA values in the 0.0 to 1.0 range. This gives us a nice grey background.

	float z=0; // z-axis position

	// load file using SFML
	sf::Image texture;
	texture.loadFromFile("glass.bmp");

	GLuint texture_handle;
	glGenTextures(1, &texture_handle); // allocate a texture handle within OpenGL
	glBindTexture(GL_TEXTURE_2D, texture_handle); // bind to the texture handle target

	// import texture into OpenGL
	glTexImage2D(
		GL_TEXTURE_2D, 0, GL_RGBA,
		texture.getSize().x, texture.getSize().y,
		0,
		GL_RGBA, GL_UNSIGNED_BYTE, texture.getPixelsPtr()
	);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glEnable(GL_TEXTURE_2D); // enable 2D textures
	glEnable(GL_DEPTH_TEST); // use the depth buffer to remove hidden surfaces to prevent their render + calculation
	glDepthMask(GL_TRUE); // enable program to write into the depth buffer

	// Setup a perspective projection & Camera position

	// GL_PROJECTION what we actually see
	glMatrixMode(GL_PROJECTION); // Select the builtin projection matrix
	glLoadIdentity();  // reset the projection matrix by loading the projection identity matrix

	GLdouble fovY = 90;
	GLdouble aspect = 1.0f;
	GLdouble zNear = 1.0f; 
	GLdouble zFar = 300.0f;

	const GLdouble pi = 3.1415926535897932384626433832795;
	GLdouble fW, fH;

	fH = tan(fovY / 360 * pi) * zNear;
	fW = fH * aspect;

	// define a perspective projection
	glFrustum(-fW, fW, -fH, fH, zNear, zFar); // multiply the set matrix; by a perspective matrix

	bool rotate = true;
	float angle = 1;


	// enable lighting effects
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);

	glShadeModel(GL_SMOOTH);

	GLfloat AMBIENT_COLOR[] = { 0.15f, 0.15f, 0.15f, 1.0f };
	//GLfloat LIGHT_POSITION[] = { 0.0f, +1.0f, 0.0f, 0.0f }; // x,y,z,w directional light
 	GLfloat LIGHT_POSITION[] = { 0.0f, -1000.0f, -3000.0f, 1.0f }; // x,y,z,w positional light
	
	GLfloat DIFFUSE_COLOR[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat SPECULAR_COLOR[] = { 1.25f, 1.25f, 1.25f, 1.0f };

	glLightfv(GL_LIGHT0, GL_POSITION, LIGHT_POSITION);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, DIFFUSE_COLOR);
	glLightfv(GL_LIGHT0, GL_SPECULAR, SPECULAR_COLOR);
	
	GLfloat LIGHT_POSITION_1[] = { 0.0f, +10000.0f, -6000.0f, 1.0f }; // x,y,z,w point light source
	GLfloat DIFFUSE_COLOR_1[] = { 1.00f, 0.1f, 0.1f, 1.0f };

	glLightfv(GL_LIGHT1, GL_POSITION, LIGHT_POSITION_1);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, DIFFUSE_COLOR_1);

	GLfloat DIFFUSE_MATERIAL_COLOR[] = { 1.0f, 1.0f, 1.0f, 1.00f };
	glMaterialfv(GL_FRONT, GL_AMBIENT, DIFFUSE_MATERIAL_COLOR);

	// Start game loop
	while (window.isOpen())
	{
		// Process events
		sf::Event Event;
		while (window.pollEvent(Event))
		{
			// Close window : exit
			if (Event.type == sf::Event::Closed)
				window.close();

			// Escape key : exit
			if ((Event.type == sf::Event::KeyPressed) && (Event.key.code == sf::Keyboard::Escape))
				window.close();

			if ((Event.type == sf::Event::KeyPressed) && (Event.key.code == sf::Keyboard::A)) {
				rotate = !rotate;
			}

			if ((Event.key.code == sf::Keyboard::Left)) {
				angle = angle - 0.1f;
			}

			if ((Event.key.code == sf::Keyboard::Right)) {
				angle = angle + 0.1f;
			}


			if ((Event.key.code == sf::Keyboard::Up)) {
				z = z + 1.0f;
			}

			if ((Event.type == sf::Event::KeyPressed) && (Event.key.code == sf::Keyboard::Down)) {
				z = z - 1.0f;
			}
		}

		//Prepare for drawing
		// Clear color and depth buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear z-buffer and set previously selected colour

		// Apply some transformations for the cube
		// The GL_MODELVIEW is used for transforming our model
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity(); // reset
		glLightfv(GL_LIGHT0,GL_POSITION, LIGHT_POSITION);
		glLightfv(GL_LIGHT1,GL_POSITION, LIGHT_POSITION_1);
		glTranslatef(0.f, 0.f, z); // position the cube model at z-position -200; ie. away from us

		std::cout << "z:" << z << std::endl;
		std::cout << "angle:" << angle << std::endl;

		// rotate x,y,z by a given angle in degrees
		glRotatef(angle * 20, 0.f, 1.f, 0.f); // rotate around the y-axis

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texture_handle); // bind to the texture handle target

		//glEnable(GL_BLEND);
		//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		glEnable(GL_CULL_FACE); // enable back face culling so we don't see the hidden vertices / textures of the cube
		glCullFace(GL_FRONT);

		//Draw a cube
		glBegin(GL_QUADS);//draw some squares

		// Back Face
		glNormal3f(0.0f, 0.0f, -1.0f); // point out of the screen
		glTexCoord2f(1.0f, 0.0f); glVertex3f(-50.0f, -50.0f, -50.0f);  // Bottom Right Of The Texture and Quad
		glTexCoord2f(1.0f, 1.0f); glVertex3f(-50.0f, 50.0f, -50.0f);  // Top Right Of The Texture and Quad
		glTexCoord2f(0.0f, 1.0f); glVertex3f(50.0f, 50.0f, -50.0f);  // Top Left Of The Texture and Quad
		glTexCoord2f(0.0f, 0.0f); glVertex3f(50.0f, -50.0f, -50.0f);  // Bottom Left Of The Texture and Quad

		// Front Face
		//glNormal3f(0.0f, 0.0f, 1.0f);
		//glTexCoord2f(0.0f, 0.0f); glVertex3f(-50.0f, -50.0f, 50.0f);  // Bottom Left Of The Texture and Quad
		//glTexCoord2f(1.0f, 0.0f); glVertex3f(50.0f, -50.0f, 50.0f);  // Bottom Right Of The Texture and Quad
		//glTexCoord2f(1.0f, 1.0f); glVertex3f(50.0f, 50.0f, 50.0f);  // Top Right Of The Texture and Quad
		//glTexCoord2f(0.0f, 1.0f); glVertex3f(-50.0f, 50.0f, 50.0f);  // Top Left Of The Texture and Quad
		
		// Top Face
		glNormal3f(0.0f, -1.0f, 0.0f); // point down
		glTexCoord2f(0.0f, 1.0f); glVertex3f(-50.0f, 50.0f, -50.0f);  // Top Left Of The Texture and Quad
		glTexCoord2f(0.0f, 0.0f); glVertex3f(-50.0f, 50.0f, 50.0f);  // Bottom Left Of The Texture and Quad
		glTexCoord2f(1.0f, 0.0f); glVertex3f(50.0f, 50.0f, 50.0f);  // Bottom Right Of The Texture and Quad
		glTexCoord2f(1.0f, 1.0f); glVertex3f(50.0f, 50.0f, -50.0f);  // Top Right Of The Texture and Quad

		// Bottom Face
		glNormal3f(0.0f, 1.0f, 0.0f); // point up
		glTexCoord2f(1.0f, 1.0f); glVertex3f(-50.0f, -50.0f, -50.0f);  // Top Right Of The Texture and Quad
		glTexCoord2f(0.0f, 1.0f); glVertex3f(50.0f, -50.0f, -50.0f);  // Top Left Of The Texture and Quad
		glTexCoord2f(0.0f, 0.0f); glVertex3f(50.0f, -50.0f, 50.0f);  // Bottom Left Of The Texture and Quad
		glTexCoord2f(1.0f, 0.0f); glVertex3f(-50.0f, -50.0f, 50.0f);  // Bottom Right Of The Texture and Quad

		// Right face
		glNormal3f(-1.0f, 0.0f, 0.0f); // point left
		glTexCoord2f(1.0f, 0.0f); glVertex3f(50.0f, -50.0f, -50.0f);  // Bottom Right Of The Texture and Quad
		glTexCoord2f(1.0f, 1.0f); glVertex3f(50.0f, 50.0f, -50.0f);  // Top Right Of The Texture and Quad
		glTexCoord2f(0.0f, 1.0f); glVertex3f(50.0f, 50.0f, 50.0f);  // Top Left Of The Texture and Quad
		glTexCoord2f(0.0f, 0.0f); glVertex3f(50.0f, -50.0f, 50.0f);  // Bottom Left Of The Texture and Quad

		// Left Face
		glNormal3f(1.0f, 0.0f, 0.0f); // point right
		glTexCoord2f(0.0f, 0.0f); glVertex3f(-50.0f, -50.0f, -50.0f);  // Bottom Left Of The Texture and Quad
		glTexCoord2f(1.0f, 0.0f); glVertex3f(-50.0f, -50.0f, 50.0f);  // Bottom Right Of The Texture and Quad
		glTexCoord2f(1.0f, 1.0f); glVertex3f(-50.0f, 50.0f, 50.0f);  // Top Right Of The Texture and Quad
		glTexCoord2f(0.0f, 1.0f); glVertex3f(-50.0f, 50.0f, -50.0f);  // Top Left Of The Texture and Quad
		glEnd();

		glEnable(GL_DEPTH_TEST);
		// Finally, display rendered frame on screen
		window.display();
	}

	return EXIT_SUCCESS;
}