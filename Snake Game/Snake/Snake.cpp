#include <iostream>
#include <random>
#include <sstream>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

const int GRID_FACTOR = 20;
const int MAX_SNAKE_SZ = 40;

bool isCollision(sf::Vector2i snake[MAX_SNAKE_SZ], int snakeIndex)
{
	bool collision = false;
	for (int i = 1; i < snakeIndex; i++)
	{
		if (snake[0] == snake[i])
		{
			collision = true;
			break;
		}
	}

	return collision;
}

void calcPosition(sf::Vector2i snake[MAX_SNAKE_SZ], int snakeIndex, sf::Vector2u size, bool left, bool right, bool up, bool down)
{
	sf::Vector2i tmp = sf::Vector2i(snake[0]);

	for (int i = snakeIndex; i > 0; i--)
	{
		snake[i] = snake[i - 1];
	}

	if (left == true)
	{
		tmp.x = tmp.x - 1;

		if (tmp.x < 0)
		{
			tmp.x = GRID_FACTOR;
		}
	}

	if (right == true)
	{
		tmp.x = tmp.x + 1;
		if (tmp.x >(GRID_FACTOR - 1))
		{
			tmp.x = 0;
		}
	}

	if (up == true)
	{
		tmp.y = tmp.y - 1;

		if (tmp.y < 0)
		{
			tmp.y = GRID_FACTOR;
		}
	}

	if (down == true)
	{
		tmp.y = tmp.y + 1;

		if (tmp.y >(GRID_FACTOR - 1))
		{
			tmp.y = 0;
		}

	}
	snake[0] = tmp;
	
}

int main()
{
	sf::Font font;
	sf::Text text;

	if (!font.loadFromFile("arial.ttf"))
	{
		// error...
	}

	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML Snake");
	window.setFramerateLimit(60);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
		
		text.setFont(font);
		text.setCharacterSize(20);
		text.setString("Hello World!");
		text.setPosition(sf::Vector2f(400,300));
		text.setColor(sf::Color::Red);
		
		window.clear();

		window.draw(text);

		window.display();
	}

	return 0;
}