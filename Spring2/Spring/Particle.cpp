#include "Particle.h"

Particle::Particle()
{
	location = sf::Vector2f(0, 0);
	velocity = sf::Vector2f(0, 0);
	acceleration = sf::Vector2f(0, 0);
	boundary = sf::FloatRect();
}

Particle::Particle(const sf::FloatRect &boundary, const  sf::Vector2f &location,const  float &mass)
{
	this->location = location;
	this->velocity = sf::Vector2f();
	this->acceleration = sf::Vector2f();
	this->boundary = boundary;
	this->mass = mass;

	this->topSpeed = 200;
}

void Particle::setVelocity(const sf::Vector2f &velocity)
{
	this->velocity = velocity;
}

void Particle::setAcceleration(const sf::Vector2f &acceleration)
{
	this->acceleration = acceleration;
}

sf::Vector2f Particle::getAcceleration()
{
	return acceleration;
}


sf::Vector2f Particle::getVelocity()
{
	return velocity;
}

void Particle::setLocation(const sf::Vector2f & location)
{
	this->location = location;
}

sf::Vector2f Particle::getLocation()
{
	return location;
}

void Particle::applyForce(const sf::Vector2f &force)
{
	acceleration = acceleration + (force / mass);
}

void Particle::update(const sf::Time &deltaTime)
{
	velocity = velocity + (acceleration * deltaTime.asSeconds());
	location = location + (velocity * deltaTime.asSeconds());

	if (life > 0)
	{
		life--;
	}

	
	//if (vm::magnitude(velocity) > topSpeed)
	//{
	//	velocity = vm::setMagnitude(velocity, topSpeed);
	//}
	
	velocity = velocity * 0.995f;

	acceleration = sf::Vector2f();
}

float Particle::getMass()
{
	return mass;
}

int Particle::getLife()
{
	return life;
}

void Particle::checkEdges()
{
}

Particle::~Particle()
{
}
