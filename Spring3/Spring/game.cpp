#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <random>
#include <list>
#include <cmath>


#include "Particle.h"
#include "Spring.h"
#include "VectorMath.h"

void processEvents(Spring &spring, const sf::FloatRect &rect, sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
	}
}

sf::VertexArray createLineVertex(Particle a, Particle b)
{
	sf::VertexArray line(sf::Lines, 2);
	line[0] = sf::Vertex(a.getLocation(), sf::Color::White);
	line[1] = sf::Vertex(b.getLocation(), sf::Color::White);

	return line;
}

sf::CircleShape createCircle(Particle particle,sf::Color color)
{
	sf::CircleShape circle = sf::CircleShape(10);
	circle.setOrigin(10, 10);
	circle.setPosition(particle.getLocation());
	circle.setFillColor(color);

	return circle;
}



int main()
{
	sf::Time TimePerFrame = sf::seconds(1.f / 60);

	sf::ContextSettings context;
	context.antialiasingLevel = 10;

	sf::RenderWindow window(sf::VideoMode(1024, 768), "SFML Springs", sf::Style::Default, context);

	const sf::FloatRect rect = sf::FloatRect(sf::Vector2f(0, 0), sf::Vector2f(window.getSize()));

	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;

	Particle particlea = Particle(rect, sf::Vector2f(window.getSize().x / 2, (window.getSize().y / 2)), 1);
	Particle particleb = Particle(rect, sf::Vector2f(window.getSize().x / 2 - 300, (window.getSize().y / 2) + 300), 1);
	Particle particlec = Particle(rect, sf::Vector2f(window.getSize().x / 2 + 300, (window.getSize().y / 2) + 300), 1);
	Particle particled = Particle(rect, sf::Vector2f(window.getSize().x / 2 + 300, (window.getSize().y / 2) + 300), 1);
	Particle particlee = Particle(rect, sf::Vector2f(window.getSize().x / 2 + 300, (window.getSize().y / 2) + 300), 1);
	Particle particlef = Particle(rect, sf::Vector2f(window.getSize().x / 2 + 300, (window.getSize().y / 2) + 300), 1);

	Spring springa = Spring(particlea, particleb, 50);
	Spring springb = Spring(particleb, particlec, 50);
	Spring springc = Spring(particlec, particlea, 50);
	Spring springd = Spring(particled, particlea, 50);
	Spring springe = Spring(particlee, particled, 50);
	Spring springf = Spring(particlee, particlec, 50);
	Spring springg = Spring(particlef, particled, 50);
	Spring springh = Spring(particlef, particlee, 50);


	while (window.isOpen())
	{
		processEvents(springa, rect, window);

		timeSinceLastUpdate += clock.restart();

		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;
			processEvents(springa, rect, window);

			// iterate a little more to speed convergance up
			for (int i = 0; i < 100; i++)
			{
				springa.update(TimePerFrame);
				springb.update(TimePerFrame);
				springc.update(TimePerFrame);
				springd.update(TimePerFrame);
				springe.update(TimePerFrame);
				springf.update(TimePerFrame);
				springg.update(TimePerFrame);
				springh.update(TimePerFrame);
			}
		}

		window.clear();

		sf::CircleShape a = createCircle(springa.getOrigin(), sf::Color::Red);		
		sf::CircleShape b = createCircle(springb.getOrigin(), sf::Color::Green);
		sf::CircleShape c = createCircle(springc.getOrigin(), sf::Color::Blue);
		sf::CircleShape d = createCircle(springd.getOrigin(), sf::Color::Magenta);
		sf::CircleShape e = createCircle(springe.getOrigin(), sf::Color::Cyan);
		sf::CircleShape f = createCircle(springg.getOrigin(), sf::Color::Yellow);

		sf::VertexArray verticesAB = createLineVertex(springa.getOrigin(), springb.getOrigin());
		sf::VertexArray verticesBC = createLineVertex(springb.getOrigin(), springc.getOrigin());
		sf::VertexArray verticesCA = createLineVertex(springc.getOrigin(), springa.getOrigin());
		sf::VertexArray verticesDA = createLineVertex(springd.getOrigin(), springa.getOrigin());
		sf::VertexArray verticesDC = createLineVertex(springd.getOrigin(), springc.getOrigin());

		window.draw(verticesAB);
		window.draw(verticesBC);
		window.draw(verticesCA);
		window.draw(verticesDA);
		window.draw(verticesDC);

		window.draw(a);
		window.draw(b);
		window.draw(c);
		window.draw(d);
		window.draw(e);
		window.draw(f);

		window.display();
	}
}

